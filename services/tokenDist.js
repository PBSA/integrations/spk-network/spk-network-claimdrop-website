const axios = require('axios')

var args = process.argv.slice(2)

//args[0] = URL to the API
//args[1] = SESSION_SALT

async function issueTokens() {
  try {
    const response = await axios.post(args[0], { apiVerify: args[1] })
    console.log(response.data)
  } catch (error) {
    console.error('\n\n%s - An error occurred in the LARYNX Token Distribution system.\nPlease contact Hiltos on Discord (Hiltos#8360) for help.\n\n', new Date())
  }
}

issueTokens()
