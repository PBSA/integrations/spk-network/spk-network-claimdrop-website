import React, {
  createContext,
  useContext,
  useState,
  useCallback,
  useMemo
} from 'react'
import useUser from '../lib/useUser';

const UserContextState = createContext();
const UserContextUpdater = createContext();

const useUserContextState = () => {
  const context = useContext(UserContextState);
  if (context === undefined) {
    throw new Error("useUserContextState was used outside of its Provider");
  }
  return context;
};

const useUserContextUpdater = () => {
  const context = useContext(UserContextUpdater);
  if (context === undefined) {
    throw new Error("useUserContextUpdater was used outside of its Provider");
  }

  return context;
};

const UserContextProvider = ({ children }) => {

  const { u } = useUser();

  // the value that will be given to the context
  const [user, setUser] = useState(u);
  const [loginMetadata, setLoginMetadata] = useState(null);

  // sign out the user, memoized
  const signout = useCallback(() => {
    setUser(null);
    setLoginMetadata(null);
  }, []);

  // sign in the user, memoized
  const signin = useCallback((u, m) => {
    setUser(u);
    setLoginMetadata(m);
  }, []);

  /*
  // fetch a user from a fake backend API
  useEffect(() => {
    const fetchUser = () => {
      // this would usually be your own backend, or localStorage
      // for example
      fetch("https://randomuser.me/api/")
        .then((response) => response.json())
        .then((result) => setUser(result.results[0]))
        .catch((error) => console.log("An error occurred"));
    };

    fetchUser();
  }, []);
  */

  // memoize the full context value
  const userInStateMemo = useMemo(() => ({
    user,
    setUser
  }), [user, setUser]);

  // memoize the full context value
  const updaterMemo = useMemo(() => ({
    signout,
    signin
  }), [signout, signin]);

  return (
    // the Provider gives access to the context to its children
    <UserContextState.Provider value={userInStateMemo}>
      <UserContextUpdater.Provider value={updaterMemo}>
        {children}
      </UserContextUpdater.Provider>
    </UserContextState.Provider>
  );
};

export { UserContextProvider, useUserContextState, useUserContextUpdater };
