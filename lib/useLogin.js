import { Client, PrivateKey } from '@hiveio/dhive'
import { useContext } from 'react'
import { useUserContextState } from '../context/usercontext'
import { Context } from '../components/layout'
import fetchJson from './fetchJson'

const useLogin = () => {

  const client = new Client([
    'https://api.hive.blog',
    'https://anyx.io',
    'https://api.openhive.network',
  ], { chainId: 'beeab0de00000000000000000000000000000000000000000000000000000000' })
  const { user, setUser } = useUserContextState()
  const { setDoneLoading, setAuthFailed } = useContext(Context)


  const login = async (username, key) => {
    
    try {
      setDoneLoading(false)
      // Step 1: convert posting key to dhive posting key.
      const dhivePostPrivKey = PrivateKey.fromString(key)
      //console.log('key!! ', dhivePostPrivKey)

      // Step 2: get current chain params.
      const props = await client.database.getDynamicGlobalProperties()
      const head_block_number = props.head_block_number
      const head_block_id = props.head_block_id
      //console.log(props)

      // Step 3: build an operation.
      const op = {
        ref_block_num: head_block_number,
        ref_block_prefix: Buffer.from(head_block_id, 'hex').readUInt32LE(4),
        expiration: new Date(Date.now() + 60 * 1000).toISOString().slice(0, -5),
        operations: [
          [
            'vote',
            {
              voter: username,
              author: 'auth',
              permlink: 'auth',
              weight: 10000,
            },
          ],
        ],
        extensions: [],
      }

      // Step 4: make a signed transaction.
      //console.log('before sign')
      const stx = client.broadcast.sign(op, dhivePostPrivKey)
      //console.log('after sign', stx)
      const body = {
        authMethod: 'Hive Posting Key',
        username: username,
        data: stx,
        keepKeychainState: user.haveKeychain
      }
      try {
        const res = await fetchJson('/api/login', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body),
        })
        if (res.callStatus < 300) {
          setDoneLoading(true)
          setUser(res)
        } else {
          setDoneLoading(true)
          setAuthFailed(true)
          console.log(res.message)
        }
      } catch (err) {
        setDoneLoading(true)
        setAuthFailed(true)
        console.log('An unexpected error happened:', err.message)
      }
    } catch (err) {
      setDoneLoading(true)
      setAuthFailed(true)
      console.log('?', err.message)
    }
  }

  async function logout() {
    try {
      setDoneLoading(false)
      const res = await fetchJson('/api/logout', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      })
      if(res.callStatus < 300) { 
        setDoneLoading(true)
        setUser(res)
      }
    }
    catch (err) {
      setDoneLoading(true)
      console.log('An unexpected error happened:', err.message)
    }
  }

  async function hiveKeychainHandshake(keyType, haveKeychain) {

    const args = {
      account: null,
      message: `Click 'Confirm' to sign in with Hive Keychain.`,
      key: keyType,
      rpc: null,
      title: 'Sign In'
    }

    try {
      if(window.hive_keychain) {
          let keychain = window.hive_keychain
          keychain.requestSignBuffer(args.account, args.message, args.key, async (r) => {
            //console.log(r)
            if(r.success){
              const body = {
                authMethod: 'Hive Keychain',
                username: r.data.username,
                data: '',
                keepKeychainState: haveKeychain
              }
              loginWithHiveKeychain(body)
            }
          }, args.rpc, args.title)
      }
      else {
        console.log('error: hive keychain is not installed.')
      }
    }
    catch(err) { console.log(err.message) }
  }

  async function loginWithHiveKeychain(body) {
    try {
      setDoneLoading(false)
      const res = await fetchJson('/api/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      })
      //console.log(res)
      if(res.callStatus < 300) { 
        setDoneLoading(true)
        setUser(res)
      }
      else {
        setDoneLoading(true)
        setAuthFailed(true)
        console.log(res.message)
        // Auth failed! Likely a wrong username or posting key.
      }
    }
    catch (err) {
      setDoneLoading(true)
      setAuthFailed(true)
      console.log('An unexpected error happened:', err.message)
    }
  }

  return {
    login,
    loginWithHiveKeychain,
    logout,
    hiveKeychainHandshake
  }
}

export default useLogin