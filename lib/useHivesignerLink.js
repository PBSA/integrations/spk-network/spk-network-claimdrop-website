import fetchJson from './fetchJson'

const useHivesignerLink = (location) => {
  
  const getHSLink = async () => {
    const hivesignerLink = await fetchJson('/api/hivesigner-link', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ location })
    })
    return hivesignerLink.link
  }

  return { getHSLink }
}

export default useHivesignerLink