import Head from 'next/head'
import { useContext, useLayoutEffect } from 'react'
import useState from 'react-usestateref'
import Button from '../button'
import Input from '../input'
import useLogin from '../../lib/useLogin'
import { useTheme } from 'next-themes'
import ProgIndicator from '../../components/progindicator'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Context } from '../../components/layout'
import { useUserContextState } from '../../context/usercontext'
import useHivesignerLink from '../../lib/useHivesignerLink'
import notify from '../toast'

const ClaimWithoutUser = (props) => {

  const { login, hiveKeychainHandshake } = useLogin()
  const { username, setUsername, postingKey, setPostingKey, doneLoading, authFailed, setAuthFailed } = useContext(Context)
  const { user } = useUserContextState()

  const [tempUsername, setTempUsername, tempUsernameRef] = useState(username)
  const [tempPostingKey, setTempPostingKey, tempPostingKeyRef] = useState(postingKey)
  const [hivesignerLink, setHivesignerLink] = useState('')

  const { theme } = useTheme()
  const dark = theme === 'dark' ? true : false

  useLayoutEffect(() => {
    if (user == null || !user.isLoggedIn) {
      const { getHSLink } = useHivesignerLink(encodeURIComponent(window?.location.href))
      getHSLink().then(r => setHivesignerLink(r))
    }
  },[user])

  function handleLogin(uname, pass) {
    login(uname, pass)
  }

  function onKeyDownNext(event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      const input = document.getElementById('auth-input-key-body')
      input.focus()
      input.select()
    }
  }

  function onKeyDownSubmit(event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      document.getElementById('auth-hive-account-body').click()
    }
  }

  function renderLoading() {
    return (
      <div className={`flex flex-grow justify-center items-center w-full h-32 animate-pulse`}>
        <FontAwesomeIcon icon={['fas', 'stopwatch']} className={`animate-bounce mr-2`} />
        Loading...
      </div>
    )
  }

  function renderHiveKeychainButton( isActive ) {
    return (
      <button id='auth-hive-keychain' className={`relative flex btn w-full mx-auto ${isActive ? `` : `btn-disabled2`}`}
        onClick={isActive ? (() => handleHiveKeychainHandshake('Posting', true)) : () => notify('Hive Keychain is not installed.', 'error', dark)}>
        <div className={`w-full font-bold text-center mb-1.5 ${isActive ? `` : `opacity-75`}`}>
          Sign in with Hive Keychain
        </div>
        <div className={`absolute text-xs bottom-0 right-1/4 -mr-1 mb-px italic ${isActive ? `` : `opacity-75`}`}>
          posting key
        </div>
      </button>
    )
  }

  function renderSupportLink() {
    return (
      <p className={`flex flex-col md:flex-row justify-end space-x-2 w-full text-center md:mt-3 mt-6 text-sm`}>
        <span>need help?</span>
        <a target={`_blank`} href={`https://discord.com/channels/793757909837676554/915369046906658886`} rel={`noopener noreferrer`}>
          <span className={`link link-underline`}>get support! <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
        </a>
      </p>
    )
  }

  return (
    <>
      <Head>
        <title>Claim SPK Network Tokens - LARYNX</title>
      </Head>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
      <div className={`space-y-10 mb-4`}>
            <h2>Your Claim Progress</h2>
            <ProgIndicator stage={0} status={'eligible'} origin={props.origin} />
          </div>
      </div>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
        <h2 className="mb-6">Step 1: Sign In with your Hive Account</h2>
        { doneLoading ? (
        <>
        <p className="text-lg mb-6">
          If your Hive account had a balance of HIVE or HIVE POWER during the
          snapshot, the new LARYNX tokens are waiting for you.
        </p>
        <div className="flex gap-5 flex-col lg:flex-row items-center">
          <div className="flex-col xl:flex-row flex flex-auto w-full gap-3">
            <div className="w-full xl:w-8/12 gap-3 flex flex-col">
              <Input
                id='auth-input-user-page'
                type='text'
                tabIndex='1'
                value={tempUsername}
                onChange={e => setTempUsername(e.target.value.toLowerCase().trim())}
                placeholder="Hive username"
                onKeyDown={onKeyDownNext.bind(this)}
              />
              <Input
                id='auth-input-key-page'
                tabIndex='2'
                value={tempPostingKey}
                onChange={e => setTempPostingKey(e.target.value)}
                placeholder="Posting key"
                onKeyDown={onKeyDownSubmit.bind(this)}
                type="password"
              />
            </div>

            <div className="w-full flex-col xl:w-4/12 items-center flex">
              <div className={`text-xs text-left font-mono leading-none self-center mb-2 ${ authFailed ? `auth-warning` : ``} opacity-0`}>
                <p>Authentication Failed.</p>
                <p>Check username & key.</p>
              </div>
              <button 
                id='auth-hive-account-body'
                tabIndex='3'
                className={`btn w-full text-white font-bold h-auto ${tempUsernameRef.current != '' && tempPostingKeyRef.current != '' ? `` : `btn-disabled2`}`}
                onClick={() => {
                  setAuthFailed(false)
                  setUsername(tempUsernameRef.current)
                  setPostingKey(tempPostingKeyRef.current)
                  handleLogin(tempUsernameRef.current, tempPostingKeyRef.current)
              }}>
                Sign In
              </button>
            </div>
          </div>

          <div className="flex flex-none w-full h-px lg:w-1/12 lg:h-150px items-center relative justify-center">
            <div className="w-full h-px lg:h-full lg:w-px dark:bg-hive-dark-grey bg-hive-light-black opacity-75" />
            <p id={'or'} className="absolute top-1/2 left-1/2 bg-hive-dark-grey dark:bg-hive-light-black p-4">
              OR
            </p>
          </div>

          <div className="w-full flex flex-auto gap-3 flex-col">
            {user != null ? renderHiveKeychainButton(user.haveKeychain) : ``}
            <a href={hivesignerLink}>
              <Button fluid>Sign In with Hive Signer</Button>
            </a>
          </div>
        </div>
        </>
        ) : renderLoading() }
        { renderSupportLink() }
      </div>
    </>
  )
}

export default ClaimWithoutUser
