import Head from 'next/head'
import { useLayoutEffect, useContext } from 'react'
import useState from 'react-usestateref'
import { useUserContextState } from '../../context/usercontext'
import fetchJson from '../../lib/fetchJson'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ProgIndicator from '../progindicator'
import { useRouter } from 'next/router'
import { Context } from '../../components/layout'
import notify from '../toast'
import { useTheme } from 'next-themes'
var Decimal = require('decimal.js-light')

const ClaimWithUser = (props) => {
  const router = useRouter()
  const { doneLoading, setDoneLoading } = useContext(Context)
  const { user } = useUserContextState()

  const [getOnce, setGetOnce] = useState(0)

  // Create Form Variables
  const [password, setPassword] = useState('')
  const [tempPeerplaysCreateUser, setTempPeerplaysCreateUser] = useState('')
  const [usernameCreateValid, setUsernameCreateValid] = useState(true)
  const [disclaimerOne, setDisclaimerOne] = useState(false)
  const [disclaimerTwo, setDisclaimerTwo] = useState(false)
  const [passFileDownloaded, setPassFileDownloaded] = useState(false)
  const [usernameTaken, setUsernameTaken] = useState(false)

  // Verify Form Variables
  const [tempPeerplaysVerifyUser, setTempPeerplaysVerifyUser] = useState('')
  const [usernameVerifyValid, setUsernameVerifyValid] = useState(false)
  const [tempPeerplaysPass, setTempPeerplaysPass] = useState('')
  const [tempPeerplaysPassValid, setTempPeerplaysPassValid] = useState(false)
  const [passVisible, setPassVisible] = useState(false)
  const [authFailed, setAuthFailed] = useState(false)
  
  // User Data
  const [userSnapData, setUserSnapData, userSnapDataRef] = useState(null)
  const [userPeerData, setUserPeerData] = useState(null)
  const [userClaimData, setUserClaimData, userClaimDataRef] = useState(null)
  const [stage, setStage, stageRef] = useState(0)

  // Claim State Data
  const [claimStage, setClaimStage, claimStageRef] = useState(0) // 0 - 13
  const [claimStageRaw, setClaimStageRaw, claimStageRawRef] = useState(0) // 0 - 25
  const [stageDate, setStageDate, stageDateRef] = useState(null)
  const [allStageDates, setAllStageDates, allStageDatesRef] = useState([])
  const [allStageDescriptions, setAllStageDescriptions, allStageDescriptionsRef] = useState([])
  
  let dateOptions = { year: 'numeric', month: 'short', day: '2-digit', hour: '2-digit', minute: '2-digit' }

  const { theme } = useTheme()
  const dark = theme === 'dark' ? true : false

  useLayoutEffect(() => {
    if(user != null && user.isLoggedIn && getOnce == 0) { 
      setGetOnce(1)
      setTempPeerplaysCreateUser(createRandomUsername(user.username))
      setPassword(createPassword())
      handleGetSettings()
      getUserData()
    }
  }, [user])

  function createRandomUsername(hiveUsername) {
    let randString = ''
    const characterList = '0123456789abcdefghijklmnopqrstuvwxyz'
    const characterListLength = characterList.length
    for (let i = 0; i < 4; i++) {
      const characterIndex = Math.round(Math.random() * characterListLength)
      randString += characterList.charAt(characterIndex)
    }
    let randUsername = 'spk.' + hiveUsername + '-' + randString
    return randUsername
  }

  function createPassword() {
    let password = ''
    const characterList = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    const characterListLength = characterList.length
    for (let i = 0; i < 52; i++) {
      const characterIndex = Math.round(Math.random() * characterListLength)
      password += characterList.charAt(characterIndex)
    }
    return password
  }

  async function handleGetSettings() {
    let a = await fetchJson(`${props.origin}/api/timing/claim-stage`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
    //console.log('a.claimStage', a.claimStage)
    //console.log('a.stageDate', a.stageDate)
    //console.log('a.allStageDates', a.allStageDates)
    //console.log('a.allStageDescriptions', a.allStageDescriptions)
    setClaimStage(Math.ceil(a.claimStage / 2)) // 0 to 13
    setClaimStageRaw(a.claimStage)
    setStageDate(a.stageDate)
    setAllStageDates(a.allStageDates.slice())
    setAllStageDescriptions(a.allStageDescriptions.slice())
  }

  async function getUserData() {
    
    try {
      setDoneLoading(false)
      if (user.username && user.status == 'eligible') {
        let u = await fetchJson(`${props.origin}/api/snapshot/u/${user.username}`, {method: 'GET'})
        if (u.data.username != null) { setStage(2) }
        setUserSnapData(u)
        //console.log('u', u)
        
        let c = await fetchJson(`${props.origin}/api/claim/u/${user.username}`, {method: 'GET'})
        setUserClaimData(c)
        //console.log('c', c)
        //console.log(userClaimDataRef.current)
        
        if (c.peerplays_verified != null && c.peerplays_verified == true) { setStage(3) }
        else {
          let p = await fetchJson(`${props.origin}/api/peerplays/u/${user.username}`, {method: 'GET'})
          setUserPeerData(p)
          //console.log('p', p)
        }

        if (claimStageRef.current == 13) { setStage(1) }
      }
      setDoneLoading(true)
    }
    catch(err) {
      setDoneLoading(true)
      console.log(err.message)
    }
  }

  async function claimNow() {
    setDoneLoading(false)
    try {
      if (user.username && user.status == 'eligible') {
        let newClaim = await fetchJson(`${props.origin}/api/claim/u/${user.username}`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(user)
        })
        setUserClaimData(newClaim)
      }
      setDoneLoading(true)
    }
    catch(err) {
      setDoneLoading(true)
      console.log(err.message)
    }
  }

  function handleUsernameValidation(val, inputToUpdate) {
    let inputValue = val.toLowerCase().trim()
    switch (inputToUpdate) {
      case 'create':
        setUsernameTaken(false)
        setUsernameCreateValid(inputValue.match(/^[a-z](?!.*([-.])\1)((?=.*(-))|(?=.*([0-9])))[a-z0-9-.]{2,62}(?<![-.])$/g))
        setTempPeerplaysCreateUser(inputValue)
        break
      case 'verify':
        setAuthFailed(false)
        setUsernameVerifyValid(inputValue.match(/^[a-z](?!.*([-.])\1)((?=.*(-))|(?=.*([0-9])))[a-z0-9-.]{2,62}(?<![-.])$/g))
        setTempPeerplaysVerifyUser(inputValue)
        break
      default:
    }
  }

  function handlePasswordValidation(val) {
    setAuthFailed(false)
    setTempPeerplaysPassValid(val.length >= 12)
    setTempPeerplaysPass(val)
  }

  function handleCheckbox(checkbox, boxToUpdate) {
    let isChecked = checkbox.checked
    switch (boxToUpdate) {
      case 'one':
        setDisclaimerOne(isChecked)
        break
      case 'two':
        setDisclaimerTwo(isChecked)
        break
      default:
    }
  }

  async function handleCreateAccount(username, pass) {
    setDoneLoading(false)
    let body = {
      username: username,
      pass: pass
    }
    let p = await fetchJson(`${props.origin}/api/peerplays/u/${user.username}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
    //console.log('p-create', p)
    if (p.usernameTaken) {
      setDoneLoading(true)
      setUsernameTaken(true)
    }
    if (p.peerplays_verified) {
      setDoneLoading(true)
      setUsernameTaken(false)
      setUserClaimData(p)
      setStage(3)
    }
  }

  async function handleVerifyAccount(username, pass) {
    setDoneLoading(false)
    let body = {
      username: username,
      pass: pass
    }
    let p = await fetchJson(`${props.origin}/api/peerplays/u/${user.username}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
    //console.log('p-verify', p)
    if (p.peerplays_verified) {
      setDoneLoading(true)
      setAuthFailed(false)
      setUserClaimData(p)
      setStage(3)
    }
    else {
      setDoneLoading(true)
      setAuthFailed(true)
    }
    
  }

  function handleDownload() {
    const element = document.createElement('a');
    const file = new Blob([password], {type: 'text/plain'});
    element.href = URL.createObjectURL(file)
    element.download = `peerplays-backup.txt`
    document.body.appendChild(element) // Required for this to work in FireFox
    element.click()
    setPassFileDownloaded(true)
  }

  function renderLoading() {
    return (
      <div className={`flex flex-grow justify-center items-center w-full h-32 animate-pulse`}>
        <FontAwesomeIcon icon={['fas', 'stopwatch']} className={`animate-bounce mr-2`} />
        Loading...
      </div>
    )
  }

  function renderClaimsLoop(haveClaims) {
    let items = []
    if (haveClaims) {
      items = userClaimDataRef.current.claims.map((claimRecord, index) => (
        <div key={claimRecord + index} className={`flex flex-col p-3 m-3 rounded bg-hive-light-grey dark:bg-hive-dark-black`}>
          <div className={`font-mono text-sm`}>
            <p>Claim Round {claimRecord.claim_round + 1}</p>
            <p>{new Decimal(claimRecord.claim_amount_claimed).toFixed(3)} LARYNX</p>
            <p>Claim Time: {claimRecord.claim_timestamp != null ? new Date(claimRecord.claim_timestamp).toLocaleString("en-US", dateOptions) : `N/A`}</p>
            <p>Status: {claimRecord.claim_amount_claimed > 0 && claimRecord.processed ? `Complete` : claimRecord.claim_amount_claimed <= 0 && claimRecord.processed ? `Unclaimed` : `Processing`}</p>
            {claimRecord.claim_timestamp != null ? (
              <p>Processed Time: { claimRecord.processed_timestamp != null ? new Date(claimRecord.processed_timestamp).toLocaleString("en-US", dateOptions) : `N/A`}</p>
            ) : (
              <p>Processed Time: N/A</p>
            )}
          </div>
        </div>
      ))
    }
    
    return items
  }

  function copyToClipboard(inputElement) {
    inputElement.select()
    inputElement.setSelectionRange(0, 99999) // For mobile devices
    navigator.clipboard.writeText(inputElement.value)
    notify('Password copied to clipboard!', 'success', dark)
  }

  function renderPeerplaysLogin() {

    let match = false
    let fullValidCreate = usernameCreateValid && disclaimerOne && disclaimerTwo && passFileDownloaded && !usernameTaken
    let fullValidVerify = usernameVerifyValid && tempPeerplaysPassValid && !authFailed

    if (userPeerData != null && userPeerData.data.name != null && userPeerData.data.name == userSnapDataRef.current.data.username) { match = true }

    return (
      <>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
        <h2 className="mb-6">Step 2: Connect or Create a Peerplays Account</h2>
        
        <div className="text-lg mb-6">
          <p>The LARYNX tokens you claim will be stored on the Peerplays blockchain. You must create a new Peerplays account or verify an existing Peerplays account to receive your claim.</p>
        </div>
        <div className="flex gap-5 flex-col xl:flex-row items-center xl:items-start">
          <div className="flex-col flex flex-auto w-full gap-3">
            <div className="w-full gap-3 flex flex-col font-mono">
              <p>Create a new Peerplays account.</p>
              <div className={`relative`}>
                <input
                  id='auth-input-create-user-page'
                  type='text'
                  value={tempPeerplaysCreateUser}
                  onChange={e => handleUsernameValidation(e.target.value, 'create')}
                  placeholder="Peerplays username"
                  className={`pr-11 rounded-lg w-full auth-form-input ${usernameCreateValid && !usernameTaken ? (``) : (`focus:ring-hive-red focus:border-hive-red border-hive-red`)}`}
                />
                {tempPeerplaysCreateUser != '' ? (
                  <div className={`absolute right-3 top-1.5 self-center`}>
                    <button id='search-clear' className={`flex mx-auto search-btn`} onClick={() => handleUsernameValidation('', 'create')}>
                      <FontAwesomeIcon icon={['fas', 'times']} className={`mt-px`} fixedWidth />
                    </button>
                  </div>
                ) : ``}
              </div>
              <div className={`relative`}>
                <input
                  id={`auth-input-generated-pass-page`}
                  className={`pr-11 rounded-lg w-full auth-form-input`}
                  value={password}
                  type='text'
                  readOnly
                />
                <div className={`absolute right-3 top-1.5 self-center`}>
                  <button id='search-clear' className={`flex mx-auto search-btn`} onClick={() => copyToClipboard(document.getElementById('auth-input-generated-pass-page'))}>
                    <FontAwesomeIcon icon={['fas', 'copy']} className={`mt-px`} fixedWidth />
                  </button>
                </div>
              </div>
              {(usernameCreateValid || tempPeerplaysCreateUser == '') && !usernameTaken ? (``) : (
                <div className={`text-hive-red text-xs p-3 rounded-lg bg-hive-light-grey dark:bg-hive-dark-black drop-shadow-md`}>
                  username must:
                  <ul>
                    {usernameTaken ? (<li>- not already be in use</li>): (``)}
                    {tempPeerplaysCreateUser.match(/^[^a-z].*/g) ? (<li>- begin with a letter</li>) : (``)}
                    {!tempPeerplaysCreateUser.match(/^((?=.*(-))|(?=.*([0-9]))).*/g) ? (<li>- contain a number or hyphen</li>) : (``)}
                    {!tempPeerplaysCreateUser.match(/^(?!.*([-.])\1).*/g) ? (<li>- not contain repeating hyphens or periods</li>) : (``)}
                    {!tempPeerplaysCreateUser.match(/^[a-z0-9-.]{2,}(?<![-.])$/g) ? (<li>- end with a number or letter</li>) : (``)}
                    {tempPeerplaysCreateUser.length < 3 || tempPeerplaysCreateUser.length > 63 ? (<li>- be 3 to 63 characters long</li>) : (``)}
                  </ul>
                </div>
              )}
              <div className={`flex flex-row w-full items-center`}>
                <input
                  id='auth-input-disclaimer-one'
                  value={disclaimerOne}
                  onChange={e => handleCheckbox(e.target, 'one')}
                  type='checkbox'
                  className={`form-checkbox rounded m-3`}
                />
                <label htmlFor={`auth-input-disclaimer-one`}>I understand that my password cannot be recovered if lost.</label>
              </div>
              <div className={`flex flex-row w-full items-center`}>
                <input
                  id='auth-input-disclaimer-two'
                  value={disclaimerTwo}
                  onChange={e => handleCheckbox(e.target, 'two')}
                  type='checkbox'
                  className={`form-checkbox rounded m-3`}
                />
                <label htmlFor={`auth-input-disclaimer-two`}>I have securely saved my generated password.</label>
              </div>
            </div>

            <div className="w-full flex-col items-center flex">
              <div className={`text-sm text-left font-mono leading-none self-center mb-2 ${ usernameTaken ? `auth-warning` : `hidden`} opacity-0`}>
                <p>Username is already in use.</p>
                <p>Please pick another username.</p>
              </div>
              <button 
                id='auth-input-download-pass-page'
                className={`btn mb-3 w-full font-bold`}
                onClick={handleDownload}>
                Download Backup {passFileDownloaded ? (<FontAwesomeIcon icon={['fas', 'check-circle']} className={`text-peerplays-green fa-sm fa-fw`} />): <span className={`text-hive-red`}>required</span>}
              </button>
              <button 
                id='auth-input-create-account-page'
                className={`btn w-full font-bold text-ellipsis overflow-hidden ${fullValidCreate ? (``) : (`btn-disabled2`)}`}
                onClick={fullValidCreate ? (() =>  handleCreateAccount(tempPeerplaysCreateUser, password)) : (() => notify('Please check your username / password / other requirements.', 'error', dark))}>
                Create{tempPeerplaysCreateUser != '' ? `: ${tempPeerplaysCreateUser}` : ``}
              </button>
            </div>
          </div>
            
          <div className="place-self-center flex flex-none w-11/12 my-4 h-px xl:w-1/12 xl:h-250px items-center relative justify-center">
            <div className="w-full h-px xl:h-full xl:w-px dark:bg-hive-dark-grey bg-hive-light-black opacity-75" />
            <p id={'or'} className="absolute top-1/2 left-1/2 bg-hive-dark-grey dark:bg-hive-light-black p-4">
              OR
            </p>
          </div>


          <div className="flex-col flex flex-auto w-full gap-3">
            <div className="w-full gap-3 flex flex-col font-mono">
              {match ? (
                <p>Verify an existing Peerplays account.<br />
                  "<span className={`italic`}>{userPeerData.data.name}</span>" was found.{` `}
                  <a onClick={() => handleUsernameValidation(userPeerData.data.name, 'verify')} className={`font-mono cursor-pointer`}>
                    <span className={`link link-underline`}>Use it?</span>
                  </a>
                </p>
              ) : (
                <p>Verify an existing Peerplays account.</p>
              )}
              <div className={`relative`}>
                <input
                  id='auth-input-verify-user-page'
                  className={`pr-11 rounded-lg w-full auth-form-input ${usernameVerifyValid && !authFailed ? (``) : (`focus:ring-hive-red focus:border-hive-red border-hive-red`)}`}
                  type='text'
                  value={tempPeerplaysVerifyUser}
                  onChange={e => handleUsernameValidation(e.target.value, 'verify')}
                  placeholder="Peerplays username"
                />
                {tempPeerplaysVerifyUser != '' ? (
                  <div className={`absolute right-3 top-1.5 self-center`}>
                    <button id='search-clear' className={`flex mx-auto search-btn`} onClick={() => handleUsernameValidation('', 'verify')}>
                      <FontAwesomeIcon icon={['fas', 'times']} className={`mt-px`} fixedWidth />
                    </button>
                  </div>
                ) : ``}
              </div>
              <div className={`relative`}>
                <input
                  id='auth-input-verify-pass-page'
                  className={`pr-20 rounded-lg w-full auth-form-input ${tempPeerplaysPassValid && !authFailed ? (``) : (`focus:ring-hive-red focus:border-hive-red border-hive-red`)}`}
                  value={tempPeerplaysPass}
                  onChange={e => handlePasswordValidation(e.target.value)}
                  placeholder={`Peerplays password`}
                  type={passVisible ? `text` : `password`}
                />
                {tempPeerplaysPass != '' ? (
                  <>
                  <div className={`absolute right-3 top-1.5 self-center`}>
                    <button id='search-clear' className={`flex mx-auto search-btn`} onClick={() => handlePasswordValidation('')}>
                      <FontAwesomeIcon icon={['fas', 'times']} className={`mt-px`} fixedWidth />
                    </button>
                  </div>
                  <div className={`absolute right-11 top-1.5 self-center`}>
                    <button id='search-clear' className={`flex mx-auto search-btn`} onClick={() => setPassVisible(!passVisible)}>
                      <FontAwesomeIcon icon={['fas', passVisible ? `eye` : `eye-slash`]} className={`mt-px`} fixedWidth />
                    </button>
                  </div>
                  </>
                ) : ``}
              </div>
              {usernameVerifyValid || tempPeerplaysVerifyUser == '' ? (``) : (
                <div className={`text-hive-red text-xs p-3 rounded-lg bg-hive-light-grey dark:bg-hive-dark-black drop-shadow-md`}>
                  username must:
                  <ul>
                    {tempPeerplaysVerifyUser.match(/^[^a-z].*/g) ? (<li>- begin with a letter</li>) : (``)}
                    {!tempPeerplaysVerifyUser.match(/^((?=.*(-))|(?=.*([0-9]))).*/g) ? (<li>- contain a number or hyphen</li>) : (``)}
                    {!tempPeerplaysVerifyUser.match(/^(?!.*([-.])\1).*/g) ? (<li>- not contain repeating hyphens or periods</li>) : (``)}
                    {!tempPeerplaysVerifyUser.match(/^[a-z0-9-.]{2,}(?<![-.])$/g) ? (<li>- end with a number or letter</li>) : (``)}
                    {tempPeerplaysVerifyUser.length < 3 || tempPeerplaysVerifyUser.length > 63 ? (<li>- be 3 to 63 characters long</li>) : (``)}
                  </ul>
                </div>
              )}
              {tempPeerplaysPassValid || tempPeerplaysPass == '' ? (``) : (
                <div className={`text-hive-red text-xs p-3 rounded-lg bg-hive-light-grey dark:bg-hive-dark-black drop-shadow-md`}>
                  password must:
                  <ul>
                    {tempPeerplaysVerifyUser.length < 12 ? (<li>- be at least 12 characters long</li>) : (``)}
                  </ul>
                </div>
              )}
            </div>

            <div className="w-full flex-col items-center flex">
              <div className={`text-sm text-left font-mono leading-none self-center mb-2 ${ authFailed ? `auth-warning` : `hidden`} opacity-0`}>
                <p>Authentication Failed.</p>
                <p>Check username & password.</p>
              </div>
              <button 
                id='auth-input-verify-account-page'
                className={`btn w-full font-bold text-ellipsis overflow-hidden ${fullValidVerify ? (``) : (`btn-disabled2`)}`}
                onClick={fullValidVerify ? (() =>  handleVerifyAccount(tempPeerplaysVerifyUser, tempPeerplaysPass)) : (() => notify('Please check your username / password.', 'error', dark))}>
                Verify{tempPeerplaysVerifyUser != '' ? `: ${tempPeerplaysVerifyUser}` : ``}
              </button>
            </div>
          </div>
        
        </div>
        { renderSupportLink() }
      </div>
      </>
    )
  }

  function renderClaimStep(claimOver = false) {
    let thisClaimStage = claimStageRef.current // 0 to 13
    let thisClaimStageIndex = thisClaimStage - 1 // shifted to get the index for the claims array.
    let thisClaim
    let haveClaimedThisRound = false
    let haveClaimedAtAll = false
    let claimedThisRound = 0
    let cpr = new Decimal(0)
    if (!claimOver) { cpr = new Decimal(userClaimDataRef.current.claimable_per_round) }
    if (userClaimDataRef.current != null && userClaimDataRef.current.claims != null && typeof userClaimDataRef.current.claims[thisClaimStageIndex] != 'undefined') { haveClaimedThisRound = true }
    if (userClaimDataRef.current != null && userClaimDataRef.current.claims != null && userClaimDataRef.current.claims.length > 0) { haveClaimedAtAll = true }
    if (haveClaimedThisRound) {
      thisClaim = userClaimDataRef.current.claims[thisClaimStageIndex]
      claimedThisRound = new Decimal(thisClaim.claim_amount_claimed)
    }
    let isStart = false
    let isUnclaimable = false
    if (allStageDescriptionsRef.current[claimStageRawRef.current].indexOf('Starts') != -1) { isStart = true }
    if (allStageDescriptionsRef.current[claimStageRawRef.current].indexOf('Hive') != -1 ||
      allStageDescriptionsRef.current[claimStageRawRef.current].indexOf('Over') != -1) { isUnclaimable = true }
    let l
    if (userSnapDataRef.current.data.Larnyx != null) { l = userSnapDataRef.current.data.Larnyx }
    if (userSnapDataRef.current.data.Larynx != null) { l = userSnapDataRef.current.data.Larynx }
    let isZeroClaim = false

    if (cpr.lessThanOrEqualTo(0)) { isZeroClaim = true }
    return (
      <>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
        <h2 className="mb-6">Step 3: Claim your tokens</h2>
        <div className="w-full flex flex-col space-y-4 md:space-y-0 md:flex-row justify-center">
          <div className="flex flex-1 flex-col items-center font-mono">
            <img className="rounded-full filter drop-shadow-lg object-contain w-100px h-100px bg-hive-light-grey mb-2" src={user?.profile?.profile_image || '/images/hive-logo.svg'} />
            <p>Hive: {user.username}</p>
            { claimOver ? `` : <p>Peerplays: {userClaimDataRef.current.peerplays_account?.peerplays_name}</p> }
            { haveClaimedAtAll ? (
              <p className={`font-mono`}>Total Claimed: {new Decimal(userClaimDataRef.current.claimed_total).toFixed(3)} LARYNX</p>
            ) : (
              <p className={`font-mono`}>Total Claimed: 0.000 LARYNX</p>
            )}
          </div>
          <div className="flex flex-1 flex-col font-mono items-center">
            {thisClaimStage >= 12 ? (
              <p>The claim drop event is over.</p>
            ) : (
              <>
              <p>claim round {thisClaimStage} of 12</p>
              <p className={`font-bold`}>{allStageDescriptionsRef.current[(thisClaimStage * 2)].replace(' Starts', '').replace(' Ends', '')}</p>
              <p>{ isStart || isUnclaimable ? `Soon` : `Currently` }, you can claim:</p>
              {userClaimDataRef.current == null || userClaimDataRef.current.claims == null ? (
                <>
                <h4>{new Decimal(l).dividedBy(12).absoluteValue().toFixed(3)} LARYNX</h4>
                <p>(0.000 claimed this round)</p>
                </>
              ) : (  
                haveClaimedThisRound == false ? (
                  <>
                  <h4>{cpr.absoluteValue().toFixed(3)} LARYNX</h4>
                  <p>(0.000 claimed this round)</p>
                  </>
                ) : (
                  <>
                  <h4>{cpr.minus(claimedThisRound).absoluteValue().toFixed(3)} LARYNX</h4>
                  <p>({claimedThisRound.toFixed(3)} claimed this round)</p>
                  </>
                )
              )}
              </>
            )}
          </div>
          <div className="flex flex-1 items-center">
            <button className={`flex flex-col btn mx-auto overflow-hidden ${thisClaimStage >= 12 || haveClaimedThisRound || isStart || isUnclaimable || isZeroClaim ? `btn-disabled2`: ``}`} onClick={thisClaimStage >= 12 || haveClaimedThisRound || isStart || isUnclaimable || isZeroClaim ? null : () => claimNow()}>
              <div className={`w-full mb-1.5`}>
                <span className={`fa-layers fa-fw ${thisClaimStage >= 12 ? `opacity-50`: ``}`}>
                  <FontAwesomeIcon icon={['fas', 'hockey-puck']} transform='shrink-8 right-6' className={`anim-coin anim-coin-1 ${thisClaimStage >= 12 ? `no-anim`: ``}`} />
                  <FontAwesomeIcon icon={['fas', 'hockey-puck']} transform='shrink-8 right-4' className={`anim-coin anim-coin-2 ${thisClaimStage >= 12 ? `no-anim`: ``}`} />
                  <FontAwesomeIcon icon={['fas', 'hockey-puck']} transform='shrink-8 right-8' className={`anim-coin anim-coin-3 ${thisClaimStage >= 12 ? `no-anim`: ``}`} />
                  <FontAwesomeIcon icon={['fas', 'hand-holding']} transform='left-3 up-1' className={`fa-2x`} />
                </span>
              </div>
              <div className={`w-full text-center ${thisClaimStage >= 12 ? `opacity-50`: ``}`}>
                <h4>Claim Now</h4>
              </div>
            </button>
          </div>
        </div>
        { renderSupportLink() }
      </div>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
        <h2 className="mb-6">Your Claim History</h2>
        { haveClaimedAtAll ? (
          <div className="w-full flex flex-wrap justify-center lg:justify-start">
            { renderClaimsLoop(true) }
          </div>
        ) : (
          <div className="w-full flex flex-wrap justify-center lg:justify-start">
            { renderClaimsLoop(false) }
          </div>
        )}
        { renderSupportLink() }
      </div>
      </>
    )
  }

  function renderClaimComplete() {
    return (
      renderClaimStep(true)
    )
  }

  return (
    <>
      <Head>
        <title>Claim SPK Network Tokens - LARYNX</title>
      </Head>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
        {user != null && user.username != null ? (
          <div className={`mb-4`}>
            <h2>Claim Progress: {user.username}</h2>
            <ProgIndicator status={user.status} origin={props.origin} userClaimData={userClaimDataRef.current}/>
          </div>
        ) : (
          <div className={`mb-4`}>
            <h2>Claim Progress: {router.query.name}</h2>
            <ProgIndicator status={'eligible'} origin={props.origin} userClaimData={null}/>
          </div>
        )}
      </div>
      { doneLoading ? ( 
          user != null && (user.status == 'blacklisted' || user.status == 'special') ? (
            <div className={`flex justify-center space-x-4 py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
              <FontAwesomeIcon icon={['fas', 'exclamation-triangle']} className={`fa-lg`} />
              <h4>Notice: This {user.status} account cannot participate in the claim drop.</h4>
            </div>
          ) : ``
        ) : ``
      }
      { doneLoading ? ( 
        stage > 0 ? ( //Do we have Snapshot Data?
          stage > 1 ? ( //Is the Claim Drop over? Stage 1 = Claim Over
            stage > 2 ? ( //Do we have Peerplays Data?
              <>{ renderClaimStep() }</>
            ) : (
              <>{ renderPeerplaysLogin() }</>
            )
          ) : (
            <>{ renderClaimComplete() }</>
          )
        ) : (
          <>
          { user != null && user.status == 'eligible' ? (
            <>
            <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
              <h2 className="mb-6">Hive User Not Found In Snapshot</h2>
              <h4>No Snapshot Data Found!</h4>
              { renderSupportLink() }
            </div>
            <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
              <h2 className="mb-6">Your Claim History</h2>
              <h4>No Previous Claim Data Found.</h4>
              { renderSupportLink() }
            </div>
            </>
          ) : `` }
          </>
        )) : (
          <>
          <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
            <h2 className="mb-6">Loading Next Step</h2>
            { renderLoading() }
          </div>
          <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black shadow-lg rounded-lg my-10`}>
            <h2 className="mb-6">Loading Your Claim History</h2>
            { renderLoading() }
          </div>
          </>
      )}
    </>
  )
}

  function renderSupportLink() {
    return (
      <p className={`flex flex-col md:flex-row justify-end space-x-2 w-full text-center md:mt-3 mt-6 text-sm`}>
        <span>need help?</span>
        <a target={`_blank`} href={`https://discord.com/channels/793757909837676554/915369046906658886`} rel={`noopener noreferrer`}>
          <span className={`link link-underline`}>get support! <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
        </a>
      </p>
    )
  }

export default ClaimWithUser
