import toast from 'react-hot-toast'
import Countdown, { zeroPad } from 'react-countdown'

export default function notify(text, type = '', dark = false) {

  // setup notifications
  let toastStyle = {}
  if (dark) {
    toastStyle = {
      background: '#2d3136',
      color: '#F0F0F8',
    }
  }

  const renderTimer = ({ days, hours, minutes, seconds, completed }) => {
    let daysString = days == 1 ? 'day' : 'days'
    let hrsString = hours == 1 ? 'hour' : 'hours'
    let minsString = minutes == 1 ? 'minute' : 'minutes'
    let secsString = seconds == 1 ? 'second' : 'seconds'
    if (!completed) {
      return (
        <div className={`flex flex-col w-full text-center items-center`}>
          <div className={`flex w-full justify-evenly text-xs space-x-4`}>
            <div className={`flex flex-col`}>
              <div className={`-mb-1`}>{zeroPad(days)}</div>
              <div>{daysString}</div>
            </div>
            <div className={`flex flex-col`}>
              <div className={`-mb-1`}>{zeroPad(hours)}</div>
              <div>{hrsString}</div>
            </div>
            <div className={`flex flex-col`}>
              <div className={`-mb-1`}>{zeroPad(minutes)}</div>
              <div>{minsString}</div>
            </div>
            <div className={`flex flex-col`}>
              <div className={`-mb-1`}>{zeroPad(seconds)}</div>
              <div>{secsString}</div>
            </div>
          </div>
        </div>
      )
    } else {
      return (<p>Time is up!</p>)
    }
  }

  switch (type) {
    case 'success':
      toast.success(text, { style: toastStyle })
    break
    case 'error':
      toast.error(text, { style: toastStyle })
    break
    case 'progindicator':

      let stageIndex = text.stageIndex
      let claimAtIndex = text.claimAtIndex.slice(1).replace('-', ' ')
      let stateAtIndex = text.stateAtIndex.slice(1)
      let stageTitle = text.stageTitleAtIndex
      let timerAtIndex = text.timerAtIndex

      if (claimAtIndex == 'claimed') { claimAtIndex = 'yes' }
      if (claimAtIndex == 'no claim' && stateAtIndex == 'current') { claimAtIndex = 'no, but available' }
      if (claimAtIndex == 'no claim' && stateAtIndex == 'complete') { claimAtIndex = 'no, unclaimed' }
      if (claimAtIndex == 'no claim' && stateAtIndex == 'incomplete') { claimAtIndex = 'not yet available' }

      if (stateAtIndex == 'pending' || stateAtIndex == 'incomplete') { stateAtIndex = 'upcoming' }

      toast((t) => (
        <>
        <div className={`flex flex-col items-center font-mono p-2 space-y-4`}>
          <p>{stateAtIndex} stage</p>
          <div className={`flex flex-col items-center`}>
            <p className={`font-bold`}>{stageTitle}{ timerAtIndex == 'complete' ? `` : `:`}</p>
            { timerAtIndex == 'complete' ? `` : <Countdown date={timerAtIndex} renderer={renderTimer} />}
          </div>
          { claimAtIndex == 'not claimable' || stateAtIndex == 'upcoming' ? `` : <p>claimed? {claimAtIndex}</p> }
          <button className={`btn btn-small font-mono mt-2`} onClick={() => toast.dismiss(t.id)}>
            OK
          </button>
        </div>
        </>
      ), { style: toastStyle, duration: 8000 })
    break
    default:
      toast(text, { style: toastStyle })
  }
}