import React, { useState, useLayoutEffect } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import AuthIndicator from './authindicator'
import ParticlesNow from './particles'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useUserContextState } from '../context/usercontext'
import useUser from '../lib/useUser'

export const Context = React.createContext()

export default function Layout({ children, home }) {

  const [haveUser, setHaveUser] = useState(false)
  const [haveKeychain, setHaveKeychain] = useState(false)
  const [doneLoading, setDoneLoading] = useState(true)
  const [authFailed, setAuthFailed] = useState(false)
  const [username, setUsername] = useState('')
  const [postingKey, setPostingKey] = useState('')

  const { user } = useUser()
  const userInStateMemo = useUserContextState()

  useLayoutEffect(() => {
    setTimeout(function(){
      if (!haveKeychain) { checkMissingKeychain() }
      //console.log('have Keychain a?', haveKeychain)
    }, 1000)

  }, [window.hive_keychain])

  useLayoutEffect(() => {
    let isSubscribed = true
    if (isSubscribed) {
      if (!haveKeychain) { checkMissingKeychain() }
      //console.log('have Keychain b?', haveKeychain)
      if(user != null) { 
        if(userInStateMemo.user == null) { 
          userInStateMemo.setUser(user)
        }
        else {
          userInStateMemo.user.isLoggedIn ? setHaveUser(true) : setHaveUser(false) 
        }
      }
    }
    return () => isSubscribed = false
  },[userInStateMemo.user, user])

  useLayoutEffect(() => {
    if (!haveKeychain) { checkMissingKeychain() }
    //console.log('have Keychain c?', haveKeychain)
    if(user != null && userInStateMemo.user != null) {
      userInStateMemo.setUser(state => ({
        ...state,
        haveKeychain: haveKeychain
      }))
    }
  }, [haveKeychain])

  function checkMissingKeychain() {
    try { 
      if(window.hive_keychain) {
        let keychain = window.hive_keychain
        keychain.requestHandshake(() => { setHaveKeychain(true) })
      } 
      else {
        setHaveKeychain(false)
      }
    }
    catch(err) { 
      console.log(err.message)
    }
  }

  return (
    <Context.Provider
      value={{
        username, setUsername,
        postingKey, setPostingKey,
        doneLoading, setDoneLoading,
        authFailed, setAuthFailed,
        checkMissingKeychain, haveKeychain
        }}>
    <div className={`flex flex-col w-full h-full min-h-screen bg-transparent`}>
      <Head>
        <link rel='icon' href='/favicon.ico' />
        <meta name='description' content='Claim LARYNX tokens for the SPK Network. Powered by Peerplays.' />
        <meta name='og:title' content='SPK Network Claimdrop' />
        <meta name='twitter:card' content='summary_large_image' />
      </Head>
      <header className={`filter drop-shadow-md z-10`}>
        <div className={`flex flex-row w-full h-32 sm:px-10 px-6 items-center justify-between md:justify-around bg-hive-light-grey dark:bg-hive-dark-black`}>
          <div className={`flex place-items-center justify-center w-full`}>
            <div className={`flex flex-1 flex-row md:justify-center`}>
              <Link href='/'><a className={`flex-shrink-0`}><img src='/images/spk-network-logo-64x64.png' className={`logo-main logo-animate object-cover`} alt={`SPK Network logo`} /></a></Link>
            </div>
            <div className={`flex flex-1 flex-col text-center justify-center md:space-x-3 md:flex-row`}>
              <h1 className={`filter drop-shadow-lg text-hive-dark-black dark:text-hive-light-grey whitespace-nowrap`}>
                SPK Network
              </h1>
              <h1 className={`filter drop-shadow-lg text-hive-dark-black dark:text-hive-light-grey whitespace-nowrap`}>
                Claim Drop
              </h1>
            </div>
            <div className={`flex flex-1 justify-end md:justify-center min-h-48px`}>
              <AuthIndicator />
            </div>
          </div>
        </div>
      </header>
      {home && (
        <div className={`flex flex-1 flex-col md:flex-row mt-2 justify-center`}>
          <img src='/images/spk-network-logo-350x350.png' className={`logo-auto w-full mx-auto md:mx-0 mb-4 md:mb-0 max-w-200px md:max-w-250px object-contain`} alt={`SPK Network logo`} />
          <div className={`flex flex-col text-center mx-auto md:mx-0 md:my-auto`}>
            { haveUser ? (<h4 className={`mb-2`}>Welcome, {userInStateMemo.user.username}!</h4>) : `` }
            <Link href={ haveUser ? `/claim/` + userInStateMemo.user.username : `/claim` }>
              <button className={`btn btn-large`}>{ haveUser ? `Go to Claim Page` : `Sign in to Claim Tokens` }</button>
            </Link>
          </div>
        </div>
      )}
      <ParticlesNow />
      <div className={`flex flex-col mt-8 mb-4 self-center w-4/5 md:w-3/5`}>
        <main>
          <div className={`fixed inset-y-0 xl:inset-x-1/4 md:inset-x-1/6 inset-x-1/16 dark:bg-black bg-white bg-opacity-30 dark:bg-opacity-30 pointer-events-none -z-10`}></div>
          {children}
        </main>
      </div>
      <footer className={`mt-auto w-full`}>
        <div className={`bg-hive-dark-grey dark:bg-hive-light-black w-full min-h-125px pt-5 md:pt-10 pb-10 shadow-inner`}>
          <div className={`place-items-center flex flex-col`}>
            <div className={`flex flex-col md:flex-row mb-4`}>
              <div className={`flex flex-1 flex-col mb-2`}>
                <div className={`flex flex-col mb-2 whitespace-nowrap text-left`}>
                  Claim LARYNX
                  <Link href={haveUser ? `/claim/` + userInStateMemo.user.username : `/claim`}><a><span className={`link link-underline`}>{haveUser ? `Go to Claim Page` : `Sign in to Claim Tokens`}</span></a></Link>
                </div>
                <div className={`flex flex-col whitespace-nowrap text-left`}>
                  Browse Snapshot
                  <Link href={`/snapshot`}><a><span className={`link link-underline`}>Browse & Search Snapshot</span></a></Link>
                </div>
              </div>
              <div className={`flex flex-1 flex-col mb-2 md:mx-10 whitespace-nowrap text-left`}>
                <div className={`flex flex-col mb-2 whitespace-nowrap text-left`}>
                  Get Help
                  <a target={`_blank`} href={`https://discord.com/channels/793757909837676554/915369046906658886`} rel={`noopener noreferrer`}>
                    <span className={`link link-underline`}>Claim Drop <FontAwesomeIcon icon={['fab', 'discord']} className={`fa-fw`} /> Channel <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
                  </a>
                </div>
                <div className={`flex flex-col whitespace-nowrap text-left`}>
                  Get SPK Desktop App
                  <a target={`_blank`} href={`https://github.com/3speaknetwork/3Speak-app/releases`} rel={`noopener noreferrer`}>
                    <span className={`link link-underline`}>Get the SPK Desktop App <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
                  </a>
                </div>
              </div>
              <div className={`flex flex-1 flex-col mb-2 whitespace-nowrap text-left`}>
                Learn More
                <a target={`_blank`} href={`https://spk.network/`} rel={`noopener noreferrer`}>
                  <span className={`link link-underline`}>spk.network website <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
                </a>
                <a target={`_blank`} href={`https://peakd.com/hive/@spknetwork/spk-network-light-paper`} rel={`noopener noreferrer`}>
                  <span className={`link link-underline`}>spk.network light paper <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
                </a>
                <a target={`_blank`} href={`https://hive.blog/@spknetwork.chat`} rel={`noopener noreferrer`}>
                  <span className={`link link-underline`}>@spknetwork.chat on <FontAwesomeIcon icon={['fab', 'hive']} className={`fa-fw`} /> <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
                </a>
                <a target={`_blank`} href={`https://hive.blog/created/ctt`} rel={`noopener noreferrer`}>
                  <span className={`link link-underline`}>#CTT tag on <FontAwesomeIcon icon={['fab', 'hive']} className={`fa-fw`} /> <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
                </a>
              </div>
            </div>
            <div className={`flex flex-row w-full justify-evenly`}>
              <a target={`_blank`} href={`https://spk.network/`} rel={`noopener noreferrer`} className={`flex-shrink-0`}>
                <img src='/images/spk-network-logo-64x64.png' className={`logo logo-animate object-cover`} alt={`SPK Network logo`} />
              </a>
              <a target={`_blank`} href={`https://hive.io/`} rel={`noopener noreferrer`} className={`flex-shrink-0`}>
                <img src='/images/hive-logo-64x64.png' className={`logo logo-animate object-cover`} alt={`Hive logo`} />
              </a>
              <a target={`_blank`} href={`https://www.peerplays.com`} rel={`noopener noreferrer`} className={`flex-shrink-0`}>
                <img src='/images/peerplays-logo-64x64.png' className={`logo logo-animate object-cover`} alt={`Peerplays logo`} />
              </a>
            </div>
          </div>
        </div>
      </footer>
    </div>
    </Context.Provider>
  )
}
