import { useState, useEffect } from 'react'
import Switch from 'react-switch'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useTheme } from 'next-themes'

const ThemeSwitch = () => {
  
  const { theme, setTheme } = useTheme();
  const dark = theme === 'dark' ? true : false;
  const [checked, setChecked] = useState(dark);
  const [mounted, setMounted] = useState(false);

  const handleChange = (nextChecked) => {
    setChecked(nextChecked);
  };

  useEffect(() => setMounted(true), []);
  useEffect(() => {
    setTheme(checked ? 'dark' : 'light');
  }, [checked, setTheme]);
  if (!mounted) return null;

  return (
    <Switch
      className={`filter drop-shadow-md hover:ring hover:brightness-125`}
      onChange={handleChange}
      checked={checked}
      aria-label="switch between day and night themes"
      offColor="#212529"
      onColor="#E7E7F1"
      onHandleColor="#2d3136"
      offHandleColor="#E7E7F1"
      handleDiameter={18}
      uncheckedIcon={
        <div className="flex justify-center items-center h-full text-sun text-base">
          <FontAwesomeIcon icon={["fas", "sun"]} className={``} />
        </div>
      }
      checkedIcon={
        <div className="flex justify-center items-center h-full text-moon text-base">
          <FontAwesomeIcon icon={["fas", "moon"]} className={``} />
        </div>
      }
      height={24}
      width={48}
    />
  );
};

export default ThemeSwitch;