const Input = ({ placeholder, value, onChange, type = 'text', ...other }) => {
  return (
    <input
      className={`rounded-lg w-full auth-form-input`}
      type={type}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      {...other}
    />
  )
}

export default Input
