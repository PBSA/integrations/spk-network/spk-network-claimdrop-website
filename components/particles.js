import Particles from "react-tsparticles";
import { useTheme } from "next-themes";

export default function ParticlesNow() {

  const { theme } = useTheme();
  const dark = theme === 'dark' ? true : false;

  const particleColor = dark ? `#F0F0F8` : `#212529`

  return (
    <Particles
      id="tsparticles"
      options={{
        fullScreen: false,
        fpsLimit: 60,
        interactivity: {
          events: {
            onClick: {
              enable: false,
              mode: "push",
            },
            onHover: {
              enable: false,
              mode: "repulse",
            },
            resize: true,
          },
          modes: {
            bubble: {
              distance: 400,
              duration: 2,
              opacity: 0.8,
              size: 40,
            },
            attract: {
              distance: 200,
              duration: 0.4,
              easing: "ease-out-quad",
              factor: 1,
              maxSpeed: 50,
              speed: 1
            },
            push: {
              quantity: 4,
            },
            repulse: {
              distance: 100,
              duration: 100,
            },
          },
        },
        particles: {
          color: {
            value: particleColor,
          },
          links: {
            color: particleColor,
            distance: 150,
            enable: true,
            opacity: 0.5,
            width: 1,
            warp: true
          },
          collisions: {
            enable: true,
          },
          move: {
            direction: "none",
            enable: true,
            outMode: "out",
            random: false,
            speed: 1,
            straight: false,
            warp: true
          },
          number: {
            density: {
              enable: true,
              value_area: 800,
            },
            value: 50,
          },
          opacity: {
            value: 0.5,
          },
          shape: {
            //type: "circle",
            options: {
              image: [{
                src: `/images/hive-logo-64x64.png`,
                replace_color: true,
                width: 32,
                height: 32
              },
              {
                src: `/images/peerplays-logo-64x64.png`,
                replace_color: true,
                width: 32,
                height: 32
              },
              {
                src: `/images/spk-network-logo-64x64.png`,
                replace_color: true,
                width: 32,
                height: 32
              }]
            },
            type: "image"
          },
          size: {
            random: {
              enable: true,
              minimumValue: 6
            },
            value: 9,
          },
        },
        detectRetina: true,
      }}
    />
  );
};