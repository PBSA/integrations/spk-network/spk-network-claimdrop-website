import { useContext, useEffect } from 'react'
import useState from 'react-usestateref'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Menu, MenuItem, MenuHeader, FocusableItem, MenuDivider } from '@szhsin/react-menu'
import { Context } from './layout'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useTheme } from 'next-themes'
import { useUserContextState } from '../context/usercontext'
import ThemeSwitch from './theme-switch'
import useHivesignerLink from '../lib/useHivesignerLink'
import useLogin from '../lib/useLogin'
import notify from './toast'

export default function AuthIndicator() {
  const router = useRouter()

  const { login, loginWithHiveKeychain, logout, hiveKeychainHandshake } = useLogin()
  const { username, setUsername, postingKey, setPostingKey, authFailed, setAuthFailed, doneLoading, checkMissingKeychain, haveKeychain } = useContext(Context)
  const { user } = useUserContextState()

  const [hivesignerLink, setHivesignerLink] = useState('')
  const [tempUsername, setTempUsername, tempUsernameRef] = useState(username)
  const [tempPostingKey, setTempPostingKey, tempPostingKeyRef] = useState(postingKey)
  const [avatarLoaded, setAvatarLoaded] = useState(false)
  const [authIndFailed, setAuthIndFailed, authFailedIndRef] = useState(false)

  const { theme } = useTheme()
  const dark = theme === 'dark' ? true : false

  useEffect(() => {
    if (user == null || !user.isLoggedIn) {
      const { getHSLink } = useHivesignerLink(encodeURIComponent(window?.location.href))
      getHSLink().then(r => setHivesignerLink(r))
    }
  },[user])

  useEffect(() => {
    setAuthIndFailed(authFailed)
  },[authFailed])

  useEffect(() => {
    //console.log(`1: effect!`)
    if(router.query.access_token && router.query.username) {
      //console.log(`2: Access_Token && Username!`)
      if(user == null || !user.isLoggedIn) {
        //console.log(`3: !user`)
        try {
          const body = {
            authMethod: 'Hivesigner',
            username: router.query.username,
            data: router.query.access_token,
            keepKeychainState: haveKeychain
          }
          loginWithHiveKeychain(body)
        }
        catch(err) {
          console.log(err.message)
        }
      }
      router.replace(router.basePath, undefined, { shallow: true })
    }
  },[router.query.access_token])

  function handleLogin(uname, pass) {
    login(uname, pass)
  }

  function handleHiveKeychainHandshake(keyType, haveKeychain) {
    hiveKeychainHandshake(keyType, haveKeychain)
  }

  function handleLogout() {
    //checkMissingKeychain()
    logout()
  }

  function onKeyDownNext(event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      const input = document.getElementById('auth-input-key')
      input.focus()
      input.select()
    }
  }

  function onKeyDownSubmit(event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      document.getElementById('auth-hive-account').click()
    }
  }

  function avatarLoader() {
    setAvatarLoaded(true)
  }

  function renderAvatar(p) {
    const pic = p?.profile_image
    const placeholderImg = () => {
      return(<img src='/images/hive-logo.svg' className={`rounded-full w-30px h-30px bg-hive-light-grey`} />)
    }
    if(pic) {
      return(
      <span className={`place-self-center mb-2`}>
        {!avatarLoaded && placeholderImg()}
        <img src={pic} className={`rounded-full object-contain w-30px h-30px bg-hive-light-grey ${avatarLoaded ? `` : `hidden`}`} onLoad={avatarLoader} />
      </span>
      )
    }
    else {
      return(placeholderImg())
    }
  }

  function renderSnapshotLink() {
    const atSnapshotPage = (router.pathname == '/snapshot') ? true : false
    if(!atSnapshotPage) {
      return(
        <FocusableItem>
        {({ ref }) => (
          <div className={`flex flex-col w-full mx-auto mt-2`}>
            <div className={`flex justify-center w-full align-middle mb-1`}>
              <FontAwesomeIcon icon={['fas', 'camera']} />
            </div>
            <div className={`w-full text-center text-sm my-auto whitespace-nowrap`}>
              Or browse the <Link href='/snapshot'><a><span className={`link link-underline`}>Snapshot</span></a></Link>.
            </div>
          </div>
        )}
        </FocusableItem>
      )
    }
    else {
      return('');
    }
  }

  function renderSiteOptions() {
    return(
      <div>
        <MenuDivider />
        <MenuHeader className={`font-semibold menu-header-fix`}>Website Options</MenuHeader>
        <FocusableItem>
        {({ ref }) => (
          <div className={`flex flex-row w-full mx-auto mt-2`}>
            <div className={`flex place-content-center w-1/2 text-right text-sm my-auto whitespace-nowrap mt-1`}>
              Theme Switch:
            </div>
            <div className={`flex justify-center w-1/2`}>
              <ThemeSwitch />
            </div>
          </div>
        )}
        </FocusableItem>
      </div>
    )
  }

  function renderClaimButton(claimPath) {
    return (
      <FocusableItem>
        {({ ref }) => (
        <Link href={claimPath}>
          <button className={`flex btn items-center w-200px mx-auto overflow-hidden`}>
            <div className={`w-1/4`}>
              <span className='fa-layers fa-fw'>
                <FontAwesomeIcon icon={['fas', 'hockey-puck']} transform='shrink-10 right-1' className='anim-coin anim-coin-1' />
                <FontAwesomeIcon icon={['fas', 'hockey-puck']} transform='shrink-10 left-1' className='anim-coin anim-coin-2' />
                <FontAwesomeIcon icon={['fas', 'hockey-puck']} transform='shrink-10 right-3' className='anim-coin anim-coin-3' />
                <FontAwesomeIcon icon={['fas', 'hand-holding']} className={`mx-1.5`} />
              </span>
            </div>
            <div className={`w-3/4 text-left text-sm`}>
              Go to Claim Page
            </div>
          </button>
        </Link>
        )}
      </FocusableItem>
    )
  }

  function renderHiveKeychainButton( isActive ) {
    return (
      <FocusableItem>
        {({ ref }) => (
          <button id='auth-hive-keychain' className={`relative flex btn w-200px mx-auto ${isActive ? `` : `btn-disabled2`}`}
            onClick={isActive ? (() => handleHiveKeychainHandshake('Posting', true)) : () => notify('Hive Keychain is not installed.', 'error', dark)}>
            <div className={`w-1/4 my-auto ${isActive ? `` : `opacity-75`}`}>
              <FontAwesomeIcon icon={['fas', 'key']} className={`place-self-center -mt-1`} />
            </div>
            <div className={`w-3/4 text-left mb-1.5 ${isActive ? `` : `opacity-75`}`}>
              Hive Keychain
            </div>
            <div className={`absolute text-xs bottom-0 right-0 mr-4 mb-px italic ${isActive ? `` : `opacity-75`}`}>
              posting key
            </div>
          </button>
        )}
      </FocusableItem>
    )
  }

  function checkUser(){
    if (doneLoading) { //Loading?
      if(user != null && user.isLoggedIn) { 
        return(renderMenuWithUser())
      }
      else { 
        return(renderMenuNoUser())
      }
    }
    else {
      return (
        <Menu 
          theming={dark ? 'dark' : undefined}
          transition={true}
          id={'auth-menu'}
          direction={'bottom'}
          align={'center'}
          position={'anchor'}
          arrow={true}
          menuButton={
            ({ open }) =>
            <button id='auth-indicator-btn' className={`btn`}>
              <span className={`align-middle`}><FontAwesomeIcon icon={['fas', 'cog']} spin className={`rounded-full`} /></span>
              <span className={`text-lg md:text-xl self-center`}>{open ? <FontAwesomeIcon icon={['fas', 'angle-up']} /> : <FontAwesomeIcon icon={['fas', 'angle-down']} />}</span>
            </button>
          }
        >
          <FocusableItem>
          {({ ref }) => (
            <div className={`flex flex-col w-full mx-auto animate-pulse`}>
              <div className={`flex w-full justify-center`}>
                <FontAwesomeIcon icon={['fas', 'stopwatch']} className={``} />
              </div>
              <div className={`w-full text-center text-sm whitespace-nowrap`}>
                Just a moment...
              </div>
            </div>
          )}
          </FocusableItem>
          {renderSiteOptions()}
        </Menu>
      )
    }
  }

  function renderMenuWithUser(){

    //console.log(user)
    let claimPath = '/claim/' + user.username
    //console.log(router.pathname)

    return (
      <Menu 
        theming={dark ? 'dark' : undefined}
        transition={true}
        id={'auth-menu'}
        direction={'bottom'}
        align={'center'}
        position={'anchor'}
        arrow={true}
        menuButton={
          ({ open }) =>
          <button id='auth-indicator-btn' className={`btn`}>
            {renderAvatar(user.profile)}
            <span className={`text-lg md:text-xl self-center`}>{open ? <FontAwesomeIcon icon={['fas', 'angle-up']} /> : <FontAwesomeIcon icon={['fas', 'angle-down']} />}</span>
          </button>
        }
        onMenuChange={e => e.open}
      >
        <MenuHeader className={`font-semibold menu-header-fix`}>Hello, {user.username}!</MenuHeader>
        <MenuDivider />
        <MenuHeader className={`text-left menu-header-fix`}>Hive Account Details</MenuHeader>
        <MenuItem disabled>
          <div className={`flex flex-col w-full text-left text-sm dark:text-hive-light-grey text-hive-dark-black font-mono`}>
            <div className={`flex w-full`}>
              <span className={`self-center w-1/12`}><FontAwesomeIcon icon={['fas', 'user']} /></span>
              <span className={`self-center w-11/12`}>: {user.username}</span>
            </div>
            <div className={`flex w-full`}>
              <span className={`self-center w-1/12`}><FontAwesomeIcon icon={['fas', 'key']} /></span>
              <span className={`self-center w-11/12`}>: {user.authMethod}</span>
            </div>
          </div>
        </MenuItem>
        {(router.pathname == '/claim/[name]' && router.query.name == user.username) ? '' : renderClaimButton(claimPath)}
        <FocusableItem>
          {({ ref }) => (
            <button id='auth-logout-btn' className={`flex items-center btn w-200px mx-auto`} onClick={() => handleLogout()}>
              <div className={`w-1/4`}>
                <FontAwesomeIcon icon={['fas', 'sign-out-alt']} className={`mx-1.5`} />
              </div>
              <div className={`w-3/4 text-left text-sm`}>
                Sign Out
              </div>
            </button>
          )}
        </FocusableItem>
        {renderSnapshotLink()}
        {renderSiteOptions()}
      </Menu>
    )
  }

  function renderMenuNoUser(){

    return (
      <Menu 
        theming={dark ? 'dark' : undefined}
        transition={true}
        id={'auth-menu'}
        direction={'bottom'}
        align={'center'}
        position={'anchor'}
        arrow={true}
        menuButton={
          ({ open }) =>
          <button id='auth-indicator-btn' className={`btn`} onClick={checkMissingKeychain}>
            <span className={`align-middle`}><FontAwesomeIcon icon={['fas', 'user-circle']} className={`rounded-full`} /></span>
            <span className={`text-lg md:text-xl self-center`}>{open ? <FontAwesomeIcon icon={['fas', 'angle-up']} /> : <FontAwesomeIcon icon={['fas', 'angle-down']} />}</span>
          </button>
        }
        onMenuChange={e => e.open && setUsername('') && setPostingKey('') && setTempUsername('') && setTempPostingKey('')}
      >
        <MenuHeader className={`font-semibold menu-header-fix`}>Sign In Options</MenuHeader>
        <MenuHeader className={`text-left menu-header-fix`}>Hive Account Details</MenuHeader>
        <FocusableItem>
        {({ ref }) => (
          <div className={`flex w-full`}>
            <span className={`self-center w-1/12`}><FontAwesomeIcon icon={['fas', 'user']} className={``} /></span>
            <input
              id='auth-input-user'
              type='text'
              tabIndex='1'
              placeholder='Hive Username'
              title='Enter your Hive account username.'
              value={tempUsernameRef.current}
              onChange={e => 
              {
                setAuthFailed(false)
                setAuthIndFailed(false)
                setTempUsername(e.target.value.toLowerCase().trim())
              }}
              onKeyDown={onKeyDownNext.bind(this)}
              className={`auth-form-input w-11/12 ml-2`}
            />
          </div>
        )}
        </FocusableItem>
        <FocusableItem>
        {({ ref }) => (
          <div className={`flex w-full`}>
            <span className={`self-center w-1/12`}><FontAwesomeIcon icon={['fas', 'pencil-alt']} className={``} /></span>
            <input
              id='auth-input-key'
              type='password'
              tabIndex='2'
              placeholder='Hive Posting Key'
              title='Enter a Hive Owner, Active, or Posting key.'
              value={tempPostingKeyRef.current}
              onChange={e => 
              {
                setAuthFailed(false)
                setAuthIndFailed(false)
                setTempPostingKey(e.target.value)
              }}
              onKeyDown={onKeyDownSubmit.bind(this)}
              className={`auth-form-input w-11/12 ml-2`}
            />
          </div>
        )}
        </FocusableItem>
        <FocusableItem>
        {({ ref }) => (
          <div className={`flex w-full`}>
            <div className={`text-xs text-left font-mono leading-none self-center mr-1 ${ authFailedIndRef.current ? `auth-warning` : `hidden`} opacity-0`}>
              <p>Authentication Failed.</p>
              <p>Check username & key.</p>
            </div>
            <button
              id='auth-hive-account'
              tabIndex='3'
              className={`btn btn-small ml-auto ${tempUsernameRef.current != '' && tempPostingKeyRef.current != '' ? `` : `btn-disabled2`}`}
              onClick={
                tempUsernameRef.current != '' && tempPostingKeyRef.current != '' ?
                (() => {
                  setAuthFailed(false)
                  setAuthIndFailed(false)
                  setUsername(tempUsernameRef.current)
                  setPostingKey(tempPostingKeyRef.current)
                  handleLogin(tempUsernameRef.current, tempPostingKeyRef.current)
                }) : (() => notify('Please check your username / password.', 'error', dark))
              }
            >
              Sign In
            </button>
          </div>
        )}
        </FocusableItem>
        <MenuHeader className={`text-left menu-header-fix`}>Or Sign In With...</MenuHeader>
        {user != null ? renderHiveKeychainButton(user.haveKeychain) : ``}
        <FocusableItem>
        {({ ref }) => (
          <a href={hivesignerLink} className={`mx-auto`} >
            <button id='auth-hive-signer' className={`flex btn w-200px`}>
              <div className={`w-1/4`}>
                <FontAwesomeIcon icon={['fab', 'hive']} className={`mx-1.5`} />
              </div>
              <div className={`w-3/4 text-left`}>
                Hive Signer
              </div>
            </button>
          </a>
        )}
        </FocusableItem>
        {renderSnapshotLink()}
        {renderSiteOptions()}
      </Menu>
    )
  }

  return ( checkUser() )
}