import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Line } from 'rc-progress'
import { useTheme } from 'next-themes'
import Countdown, { zeroPad } from 'react-countdown'
import { useEffect } from 'react'
import useState from 'react-usestateref'
import fetchJson from '../lib/fetchJson'
import { useRouter } from 'next/router'
import notify from './toast'

export default function ProgIndicator(props) {

  /*
  * ProgIndicator props:
  * 
  * props.origin = the base URL for the API.
  * 
  * props.userClaimData = used to see if the user has claims for each drop or not.
  * 
  * props.status = one of 'eligible', 'blacklisted', or 'special'
  *   only eligible users can claim tokens. So if blacklisted or special,
  *   progindicator should be grayscale but still show the current stage.
  * 
  */

  const router = useRouter()
  const { theme } = useTheme()
  const dark = theme === 'dark' ? true : false

  const [doneLoading, setDoneLoading, doneLoadingRef] = useState(false)
  const [targetPercent, setTargetPercent, targetPercentRef] = useState(0)
  const [progStateClass, setProgStateClass, progStateClassRef] = useState([
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete',
    '-incomplete'
  ])
  const [progClaimClass, setProgClaimClass, progClaimClassRef] = useState([
    '-not-claimable',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-no-claim',
    '-not-claimable'
  ])
  const [finalStateClass, setFinalStateClass, finalStateClassRef] = useState([])
  const [userClaimData, setUserClaimData, userClaimDataRef] = useState(props.userClaimData)
  const [claimStage, setClaimStage, claimStageRef] = useState(0)
  const [stageDate, setStageDate, stageDateRef] = useState(null)
  const [allStageDates, setAllStageDates, allStageDatesRef] = useState([])
  const [allStageDescriptions, setAllStageDescriptions, allStageDescriptionsRef] = useState([])

  useEffect(() => {
    setUserClaimData(props.userClaimData)
    handleGetSettings()
  }, [props.userClaimData])

  async function handleGetSettings() {
    setDoneLoading(false)
    let a = await fetchJson(`${props.origin}/api/timing/claim-stage`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
    //console.log('a.claimStage', a.claimStage)
    //console.log('a.stageDate', a.stageDate)
    //console.log('a.allStageDates', a.allStageDates)
    //console.log('a.allStageDescriptions', a.allStageDescriptions)
    setClaimStage(a.claimStage)
    setStageDate(a.stageDate)
    setAllStageDates(a.allStageDates.slice())
    setAllStageDescriptions(a.allStageDescriptions.slice())

    calcState(Math.ceil(a.claimStage / 2)) // 0 is countdown to snapshot, then 1-12 for monthly drops, then 13 is for Claim Over state.
    setDoneLoading(true)
  }

  function calcState(stage) {

    let haveClaims = false
    if (userClaimDataRef.current != null && userClaimDataRef.current.claims != null && userClaimDataRef.current.claims.length > 0) { haveClaims = true }

    let isStart = false
    let isEnd = false
    if (allStageDescriptionsRef.current[claimStageRef.current].indexOf('Starts') != -1) { isStart = true }
    if (allStageDescriptionsRef.current[claimStageRef.current].indexOf('Ends') != -1) { isEnd = true }

    let tempClaims = []
    let tempArr = []
    let tempPct = 0
    for (let i = 0; i <= 13; i++) {
      if (haveClaims) {
        if (i == 0 || i == 13) { tempClaims.push('-not-claimable') }
        else {
          if (userClaimDataRef.current.claims[i - 1] != null) {
            if (userClaimDataRef.current.claims[i - 1].claim_amount_claimed > 0) {
              tempClaims.push('-claimed')
            }
            else {
              tempClaims.push('-no-claim')
            }
          }
          else {
            tempClaims.push('-no-claim')
          }
        }
      }
      if (i == stage) {
        if (isStart) { tempArr.push('-pending') }
        else { tempArr.push('-current') }
      }
      if (i < stage) {
        tempPct += 7.692307
        tempArr.push('-complete')
      }
      if (i > stage) {
        tempArr.push('-incomplete')
      }

    }
    //console.log(tempArr)
    setTargetPercent(tempPct)
    setProgStateClass(tempArr.slice())
    if (haveClaims) { setProgClaimClass(tempClaims.slice()) }
    let tempFinal = []
    tempArr.map(function(val, index){ 
      let tempVal
      if (haveClaims) { tempVal = 'prog-state' + val + tempClaims[index] }
      else { tempVal = 'prog-state' + val + progClaimClassRef.current[index] }
      tempFinal.push(tempVal)
    })
    setFinalStateClass(tempFinal.slice())
    //console.log(targetPercent)
    //console.log(progStateClass)
    //console.log(progClaimClass)
  }

  function giveDetails(stageIndex) {
    let stateAtIndex = progStateClassRef.current[stageIndex]
    let claimAtIndex = progClaimClassRef.current[stageIndex]
    let stageTitleAtIndex
    let timerAtIndex

    if (stateAtIndex == '-complete') {
      timerAtIndex = 'complete'
      stageTitleAtIndex = allStageDescriptionsRef.current[(stageIndex * 2)].replace(' Starts', '').replace(' Ends', '')
    } // Timeframe has already ended.
    if (stateAtIndex == '-current') { 
      timerAtIndex = stageDateRef.current
      stageTitleAtIndex = allStageDescriptionsRef.current[(stageIndex * 2)]
    } // Show time it will end.
    if (stateAtIndex == '-pending' || stateAtIndex == '-incomplete') {
      timerAtIndex = allStageDatesRef.current[(stageIndex * 2) - 1]
      stageTitleAtIndex = allStageDescriptionsRef.current[(stageIndex * 2) - 1]
    } // Show time it will begin.
    if (stageIndex == 13) {
      timerAtIndex = allStageDatesRef.current[(stageIndex * 2) - 2] // The end of the December Drop is also the "beginning" of the end of then total claim drop event.
      stageTitleAtIndex = allStageDescriptionsRef.current[(stageIndex * 2) - 1]
    } // Show time it will begin.

    let notifyInput = {
      stageIndex,
      stateAtIndex,
      claimAtIndex,
      stageTitleAtIndex,
      timerAtIndex
    }
    notify(notifyInput, 'progindicator', dark)
  }

  const renderTimer = ({ days, hours, minutes, seconds, completed }) => {
    if (!completed) {
      return (<>{ renderCountdown(allStageDescriptionsRef.current[claimStageRef.current], days, hours, minutes, seconds) }</>)
    } else {
      router.reload(window.location.pathname)
      return (<p>Timeframe ended</p>)
    }
  }
  
  function renderCountdown(message, ...params) {
    let daysString = params[0] == 1 ? 'day' : 'days'
    let hrsString = params[1] == 1 ? 'hour' : 'hours'
    let minsString = params[2] == 1 ? 'minute' : 'minutes'
    let secsString = params[3] == 1 ? 'second' : 'seconds'
    return (
      <div className={`flex flex-col w-full text-center items-center`}>
        <h6 className={`mb-3`}>{message}</h6>
        <div className={`flex w-full lg:w-1/2 justify-evenly text-xs lg:text-base`}>
          <div className={`flex flex-col`}>
            <div className={`-mb-1`}>{zeroPad(params[0])}</div>
            <div>{daysString}</div>
          </div>
          <div className={`flex flex-col`}>
            <div className={`-mb-1`}>{zeroPad(params[1])}</div>
            <div>{hrsString}</div>
          </div>
          <div className={`flex flex-col`}>
            <div className={`-mb-1`}>{zeroPad(params[2])}</div>
            <div>{minsString}</div>
          </div>
          <div className={`flex flex-col`}>
            <div className={`-mb-1`}>{zeroPad(params[3])}</div>
            <div>{secsString}</div>
          </div>
        </div>
      </div>
    )
  }

  if (doneLoadingRef.current && userClaimDataRef.current != null) {
    return (
      <>
      <div className={`relative mb-4`} >
      <div className={`absolute left-0 top-0 w-1/4 h-5/6 bg-gradient-to-r from-hive-dark-grey dark:from-hive-light-black pointer-events-none z-1`}></div>
      <div className={`absolute right-0 top-0 w-1/4 h-5/6 bg-gradient-to-l from-hive-dark-grey dark:from-hive-light-black pointer-events-none z-1`}></div>
      <div className={`py-1 overflow-x-auto overflow-y-visible touch-pan-x scroll-pl-24 scroll-pr-24 snap-x`}>
      
        <div className={`mx-auto filter drop-shadow-md mb-6 mt-16 pl-24 pr-24 w-800px ${props.status != 'eligible' ? `opacity-50 grayscale` : ``}`}>
          <div className={`relative w-full flex justify-center items-center`}>
            <div className={`flex flex-wrap w-full justify-between absolute`}>
              {['Snapshot', 'Round 1', 'Round 2', 'Round 3', 'Round 4', 'Round 5', 'Round 6', 'Round 7', 'Round 8', 'Round 9', 'Round 10', 'Round 11', 'Round 12', 'Claim Over'].map((value, i) => (
                <div className={`flex relative`} key={value + i.toString()}>
                  <div className={`absolute text-xs -rotate-45 -mt-8 -ml-2 whitespace-nowrap ${i == 13 ? `-mt-9` : ``}`}>{value}</div>
                  <span className={`prog-node prog-state ${finalStateClassRef.current[i]}`}>
                    <FontAwesomeIcon
                      icon={['fas', 'circle']}
                      className={`prog-state-icon${progStateClassRef.current[i] == `-current` || progStateClassRef.current[i] == `-pending` ? `-current` : ``}`}
                    />
                    <FontAwesomeIcon
                      icon={['fas', 'circle']}
                      className={`relative inline-flex snap-start`}
                    />
                  </span>
                </div>
              ))}
            </div>
            <Line
              percent={targetPercentRef.current}
              strokeWidth={2.0}
              trailWidth={1.0}
              strokeColor={dark ? `#0148BE` : `#1084FF`}
              trailColor={`#E31337`}
              strokeLinecap={`round`}
            />
          </div>
        </div>
      </div>
      </div>
      <div className={`flex w-full justify-center`}>
            {stageDateRef.current != null ? <Countdown date={stageDateRef.current} renderer={renderTimer} /> : ``}
      </div>
      </>
    )
  }
  else {
    return (
      <>
      <div className={`relative mb-4`} >
      <div className={`absolute left-0 top-0 w-1/4 h-5/6 bg-gradient-to-r from-hive-dark-grey dark:from-hive-light-black pointer-events-none z-1`}></div>
      <div className={`absolute right-0 top-0 w-1/4 h-5/6 bg-gradient-to-l from-hive-dark-grey dark:from-hive-light-black pointer-events-none z-1`}></div>
      <div className={`py-1 overflow-x-auto overflow-y-visible touch-pan-x scroll-pl-24 scroll-pr-24 snap-x`}>
      
        <div className={`mx-auto filter drop-shadow-md mb-6 mt-16 pl-24 pr-24 w-800px opacity-50 grayscale`}>
          <div className={`relative w-full flex justify-center items-center`}>
            <div className={`flex flex-wrap w-full justify-between absolute`}>
              {['Snapshot', 'Round 1', 'Round 2', 'Round 3', 'Round 4', 'Round 5', 'Round 6', 'Round 7', 'Round 8', 'Round 9', 'Round 10', 'Round 11', 'Round 12', 'Claim Over'].map((value, i) => (
                <div className={`flex relative`} key={value + i.toString()}>
                  <div className={`absolute text-xs -rotate-45 -mt-8 -ml-2 whitespace-nowrap ${i == 13 ? `-mt-9` : ``}`}>{value}</div>
                  <span className={`prog-node prog-state`}>
                    <FontAwesomeIcon
                      icon={['fas', 'circle']}
                      className={`prog-state-icon${progStateClassRef.current[i] == `-current` || progStateClassRef.current[i] == `-pending` ? `-current` : ``}`}
                    />
                    <FontAwesomeIcon
                      icon={['fas', 'circle']}
                      className={`relative inline-flex snap-start hover:scale-125 cursor-pointer`}
                    />
                  </span>
                </div>
              ))}
            </div>
            <Line
              percent={targetPercentRef.current}
              strokeWidth={2.0}
              trailWidth={1.0}
              strokeColor={dark ? `#0148BE` : `#1084FF`}
              trailColor={`#E31337`}
              strokeLinecap={`round`}
            />
          </div>
        </div>
      </div>
      </div>
      <div className={`flex w-full justify-center`}>
            {stageDateRef.current != null ? <Countdown date={stageDateRef.current} renderer={renderTimer} /> : ``}
      </div>
      </>
    )
  }
}