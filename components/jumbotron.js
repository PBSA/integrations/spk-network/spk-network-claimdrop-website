import Link from 'next/link'
import Countdown, {zeroPad} from 'react-countdown'

export default function Jumbotron() {

  const Completionist = () => <span>You are good to go!</span>

  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    
    if (completed) {
      // Render a completed state
      return <Completionist />
    } else {
      // Render a countdown
      let daysString = (days == 1) ? "day" : "days"
      let hrsString = (hours == 1) ? "hour" : "hours"
      let minsString = (minutes == 1) ? "minute" : "minutes"
      let secsString = (seconds == 1) ? "second" : "seconds"
      return (
        <div className={`text-lg sm:text-xl`}>
          <p>Hive Blockchain Snapshot:</p>
          <p className={`hidden sm:block`}>{zeroPad(days)} {daysString}, {zeroPad(hours)} {hrsString}, {zeroPad(minutes)} {minsString}, {zeroPad(seconds)} {secsString}</p>
          <p className={`block sm:hidden`}>{zeroPad(days)} : {zeroPad(hours)} : {zeroPad(minutes)} : {zeroPad(seconds)}</p>
        </div>
      )
    }
  }

  return (
    <div className={`w-full h-auto px-10 pt-3 pb-5 bg-hive-dark-grey dark:bg-hive-light-black dark:text-hive-light-grey`}>
      <div className={`flex flex-col mx-auto w-full sm:w-3/4 text-center place-items-center justify-around`}>
        <div>
          <p className={`mt-1`}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div className={`flex flex-col sm:flex-row place-items-center w-full mt-2`}>
          <div className={`w-full sm:w-2/5 text-center`}>
            <Link href="/claim">
              <button className={`btn btn-large`}>Claim Tokens</button>
            </Link>
          </div>
          <div className={`w-full sm:w-3/5 text-center`}><Countdown date={`2022-01-06T00:00:00.000Z`} renderer={renderer} /></div>
        </div>
      </div>
    </div>
  )
}
