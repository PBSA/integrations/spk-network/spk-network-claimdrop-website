
const Button = ({ children, fluid = false, ...other }) => {
  return (
    <button style={{ height: 'max-content' }} className={`btn text-white font-bold h-auto ${fluid ? 'w-full' : ''}`} {...other}>
      {children}
    </button>
  )
}

export default Button
