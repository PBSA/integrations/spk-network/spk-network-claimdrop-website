import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-pagination-wizard'

const ClaimUserSchema = new mongoose.Schema({
  hive_name: String,
  claims: [{
    claim_timestamp: { type: Date, default: Date.now },
    claim_available_in_round: { type: Number, default: 0 },
    claim_amount_claimed: { type: Number, default: 0 },
    claim_round: { type: Number, default: 0 },
    processed: { type: Boolean, default: false },
    processed_timestamp: Date
  }],
  claimable_total: { type: Number, default: 0 },
  claimable_per_round: { type: Number, default: 0 },
  unclaimed_total: { type: Number, default: 0 },
  claimed_total: { type: Number, default: 0 },
  unclaimed_rounds: { type: Number, default: 0 },
  claimed_rounds: { type: Number, default: 0 },
  peerplays_account: {
    peerplays_name: String,
    creation_timestamp: Date,
    owner_pubkey: String,
    active_pubkey: String,
    memo_pubkey: String
  },
  peerplays_verified: { type: Boolean, default: false },
  peerplays_larynx: { type: Number, default: 0 }
}, { collection: 'claims' })

ClaimUserSchema.plugin(mongoosePaginate)

module.exports = mongoose.models.ClaimUser || mongoose.model('ClaimUser', ClaimUserSchema)