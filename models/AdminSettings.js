import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-pagination-wizard'

const AdminSettingsSchema = new mongoose.Schema({
  name: { type: String, default: 'adminsettings' },
  ADMIN_ACCOUNTS: [String],
  BLACKLISTED_ACCOUNTS: [String],
  SPECIAL_ACCOUNTS: [String],
  API_ENDPOINT_FETCH_CLAIM_BALANCES: String,
  API_ENDPOINT_PEERPLAYS_NODE: String,
  API_ENDPOINT_PEERPLAYS_FAUCET: String,
  KEY_PREFIX: String,
  SNAPSHOT_DATETIME: String,
  ICD_JAN_START_DATETIME: String,
  ICD_JAN_END_DATETIME: String,
  MCD_FEB_START_DATETIME: String,
  MCD_FEB_END_DATETIME: String,
  MCD_MAR_START_DATETIME: String,
  MCD_MAR_END_DATETIME: String,
  MCD_APR_START_DATETIME: String,
  MCD_APR_END_DATETIME: String,
  MCD_MAY_START_DATETIME: String,
  MCD_MAY_END_DATETIME: String,
  MCD_JUN_START_DATETIME: String,
  MCD_JUN_END_DATETIME: String,
  MCD_JUL_START_DATETIME: String,
  MCD_JUL_END_DATETIME: String,
  MCD_AUG_START_DATETIME: String,
  MCD_AUG_END_DATETIME: String,
  MCD_SEP_START_DATETIME: String,
  MCD_SEP_END_DATETIME: String,
  MCD_OCT_START_DATETIME: String,
  MCD_OCT_END_DATETIME: String,
  MCD_NOV_START_DATETIME: String,
  MCD_NOV_END_DATETIME: String,
  MCD_DEC_START_DATETIME: String,
  MCD_DEC_END_DATETIME: String,
}, { collection: 'adminsettings' })

AdminSettingsSchema.plugin(mongoosePaginate)

module.exports = mongoose.models.AdminSettings || mongoose.model('AdminSettings', AdminSettingsSchema)