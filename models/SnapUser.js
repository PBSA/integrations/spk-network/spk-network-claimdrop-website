import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-pagination-wizard'

const SnapUserSchema = new mongoose.Schema({
  "id": {
    "type": "Number"
  },
  "name": {
    "type": "String"
  },
  "owner": {
    "weight_threshold": {
      "type": "Number"
    },
    "account_auths": {
      "type": "Array"
    },
    "key_auths": {
      "type": [
        "Array"
      ]
    }
  },
  "active": {
    "weight_threshold": {
      "type": "Number"
    },
    "account_auths": {
      "type": "Array"
    },
    "key_auths": {
      "type": [
        "Array"
      ]
    }
  },
  "posting": {
    "weight_threshold": {
      "type": "Number"
    },
    "account_auths": {
      "type": "Array"
    },
    "key_auths": {
      "type": [
        "Array"
      ]
    }
  },
  "memo_key": {
    "type": "String"
  },
  "json_metadata": {
    "type": "String"
  },
  "posting_json_metadata": {
    "type": "String"
  },
  "proxy": {
    "type": "String"
  },
  "last_owner_update": {
    "type": "Date"
  },
  "last_account_update": {
    "type": "Date"
  },
  "created": {
    "type": "Date"
  },
  "mined": {
    "type": "Boolean"
  },
  "recovery_account": {
    "type": "String"
  },
  "last_account_recovery": {
    "type": "Date"
  },
  "reset_account": {
    "type": "String"
  },
  "comment_count": {
    "type": "Number"
  },
  "lifetime_vote_count": {
    "type": "Number"
  },
  "post_count": {
    "type": "Number"
  },
  "can_vote": {
    "type": "Boolean"
  },
  "voting_manabar": {
    "current_mana": {
      "type": "String"
    },
    "last_update_time": {
      "type": "Number"
    }
  },
  "downvote_manabar": {
    "current_mana": {
      "type": "Number"
    },
    "last_update_time": {
      "type": "Number"
    }
  },
  "balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "savings_balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "hbd_balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "hbd_seconds": {
    "type": "Number"
  },
  "hbd_seconds_last_update": {
    "type": "Date"
  },
  "hbd_last_interest_payment": {
    "type": "Date"
  },
  "savings_hbd_balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "savings_hbd_seconds": {
    "type": "Number"
  },
  "savings_hbd_seconds_last_update": {
    "type": "Date"
  },
  "savings_hbd_last_interest_payment": {
    "type": "Date"
  },
  "savings_withdraw_requests": {
    "type": "Number"
  },
  "reward_hbd_balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "reward_hive_balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "reward_vesting_balance": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "reward_vesting_hive": {
    "amount": {
      "type": "String"
    },
    "precision": {
      "type": "Number"
    },
    "nai": {
      "type": "String"
    }
  },
  "vesting_shares": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "delegated_vesting_shares": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "received_vesting_shares": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "vesting_withdraw_rate": {
    "asset": {
      "asset": {
        "type": "String"
      },
      "precision": {
        "type": "Number"
      },
      "id": {
        "type": "String"
      },
      "symbol": {
        "type": "String"
      }
    },
    "amount": {
      "type": "String"
    },
    "symbol": {
      "type": "String"
    }
  },
  "post_voting_power": {
    "amount": {
      "type": "String"
    },
    "precision": {
      "type": "Number"
    },
    "nai": {
      "type": "String"
    }
  },
  "next_vesting_withdrawal": {
    "type": "Date"
  },
  "withdrawn": {
    "type": "Number"
  },
  "to_withdraw": {
    "type": "Number"
  },
  "withdraw_routes": {
    "type": "Number"
  },
  "pending_transfers": {
    "type": "Number"
  },
  "curation_rewards": {
    "type": "Number"
  },
  "posting_rewards": {
    "type": "Number"
  },
  "proxied_vsf_votes": {
    "type": [
      "Number"
    ]
  },
  "witnesses_voted_for": {
    "type": "Number"
  },
  "last_post": {
    "type": "Date"
  },
  "last_root_post": {
    "type": "Date"
  },
  "last_post_edit": {
    "type": "Date"
  },
  "last_vote_time": {
    "type": "Date"
  },
  "post_bandwidth": {
    "type": "Number"
  },
  "pending_claimed_accounts": {
    "type": "Number"
  },
  "open_recurrent_transfers": {
    "type": "Number"
  },
  "is_smt": {
    "type": "Boolean"
  },
  "delayed_votes": {
    "type": "Array"
  },
  "governance_vote_expiration_ts": {
    "type": "Date"
  }
}, { collection: 'accounts' })

SnapUserSchema.plugin(mongoosePaginate)

module.exports = mongoose.models.SnapUser || mongoose.model('SnapUser', SnapUserSchema)