import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-pagination-wizard'

const ClaimStateSchema = new mongoose.Schema({
  record_id: String,
  claim_stage: Number,
  claim_stage_date: String,
  claim_round: Number,
  issued_to_date: Number,
  unclaimed_to_date: Number,
  unclaimed_issued_to_issuer: Number
}, { collection: 'claimstate' })

ClaimStateSchema.plugin(mongoosePaginate)

module.exports = mongoose.models.ClaimState || mongoose.model('ClaimState', ClaimStateSchema)