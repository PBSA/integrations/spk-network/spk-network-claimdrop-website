import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-pagination-wizard'

const LogSchema = new mongoose.Schema({
  name: String,
  timestamp: { type: Date, default: Date.now },
  action: String
}, { collection: 'logs' })

LogSchema.plugin(mongoosePaginate)

module.exports = mongoose.models.Log || mongoose.model('Log', LogSchema)