FROM node:16.13-alpine
  
RUN apk update && apk add \
    g++ \
    make \
    nasm \
    git \
    libtool \
    autoconf \
    automake \
    libpng-dev \
    pkgconfig

WORKDIR /app
COPY package*.json /app/
RUN npm install

COPY ./ /app/
RUN npm run build

EXPOSE 3000

ENTRYPOINT ["npm", "start"]