module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // false or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: [
        'ui-sans-serif',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: ['ui-serif', 'Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
      mono: [
        'ui-monospace',
        'SFMono-Regular',
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace',
      ],
      'nexa': ['Nexa']
     },
    extend: {
      boxShadow: {
        'inner-md': 'inset 0 4px 6px 0 rgba(0, 0, 0, 0.06)',
        'inner-lg': 'inset 0 10px 15px 0 rgba(0, 0, 0, 0.06)'
      },
      zIndex: {
        '9': '9',
        '1': '1',
        '-10': '-10',
        '-20': '-20',
        '-100': '-100',
      },
      margin: {
        '-22': '-5.5rem',
       },
      width: {
        '1920px': '1920px',
        '800px': '800px',
        '560px': '560px',
        '350px': '350px',
        '200px': '200px',
        '150px': '150px',
        '128px': '128px',
        '125px': '125px',
        '100px': '100px',
        '64px': '64px',
        '48px': '48px',
        '32px': '32px',
        '30px': '30px',
        '16px': '16px',
        '12px': '12px'
      },
      minWidth: {
        '200px': '200px',
        '220px': '220px'
      },
      maxWidth: {
        '560px': '560px',
        '350px': '350px',
        '300px': '300px',
        '250px': '250px',
        '200px': '200px',
        '100px': '100px'
      },
      height: {
        '350px': '350px',
        '300px': '300px',
        '250px': '250px',
        '200px': '200px',
        '150px': '150px',
        '128px': '128px',
        '125px': '125px',
        '100px': '100px',
        '64px': '64px',
        '48px': '48px',
        '34px': '34px',
        '32px': '32px',
        '30px': '30px',
        '16px': '16px',
        '12px': '12px',
        '7/12': '58.333333%'
      },
      minHeight: {
        '125px': '125px',
        '96px': '96px',
        '48px': '48px'
      },
      inset: {
        '1/6': '16.666667%',
        '1/8': '12.5%',
        '1/16': '6.25%'
      },
      lineHeight: {
        '0': '0'
      },
      colors: {
        sun: {
          DEFAULT: `#FFD700`
        },
        moon: {
          DEFAULT: `#0148BE`
        },
        hive: {
          'red': '#E31337',
          'dark-red': '#840B1F',
          'light-black': '#2d3136',
          'dark-black': '#212529',
          'light-grey': '#F0F0F8',
          'dark-grey': '#E7E7F1'
        },
        spknetwork: {
          'dark-blue': '#2E3886',
          'light-blue': '#6BC5D7',
          'red': '#E32334'
        },
        peerplays: {
          'light-blue': '#1084FF',
          'dark-blue': '#0148BE',
          'orange': '#FB7A14',
          'green': '#2ADF5D'
        }
      },
      gridTemplateRows: {
        '2-overlap': 'repeat(2, 0)'
      },
      backgroundImage: {
        'body-texture-light': "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNiIgaGVpZ2h0PSI2Ij4KPHJlY3Qgd2lkdGg9IjYiIGhlaWdodD0iNiIgZmlsbD0iI2VlZSI+PC9yZWN0Pgo8ZyBpZD0iYyI+CjxyZWN0IHdpZHRoPSIzIiBoZWlnaHQ9IjMiIGZpbGw9IiNlNmU2ZTYiPjwvcmVjdD4KPHJlY3QgeT0iMSIgd2lkdGg9IjMiIGhlaWdodD0iMiIgZmlsbD0iI2Q4ZDhkOCI+PC9yZWN0Pgo8L2c+Cjx1c2UgeGxpbms6aHJlZj0iI2MiIHg9IjMiIHk9IjMiPjwvdXNlPgo8L3N2Zz4=')",
        'body-texture-dark': "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNSIgaGVpZ2h0PSIxNSI+CjxyZWN0IHdpZHRoPSI1MCIgaGVpZ2h0PSI1MCIgZmlsbD0iIzI4MjgyOCI+PC9yZWN0Pgo8Y2lyY2xlIGN4PSIzIiBjeT0iNC4zIiByPSIxLjgiIGZpbGw9IiMzOTM5MzkiPjwvY2lyY2xlPgo8Y2lyY2xlIGN4PSIzIiBjeT0iMyIgcj0iMS44IiBmaWxsPSJibGFjayI+PC9jaXJjbGU+CjxjaXJjbGUgY3g9IjEwLjUiIGN5PSIxMi41IiByPSIxLjgiIGZpbGw9IiMzOTM5MzkiPjwvY2lyY2xlPgo8Y2lyY2xlIGN4PSIxMC41IiBjeT0iMTEuMyIgcj0iMS44IiBmaWxsPSJibGFjayI+PC9jaXJjbGU+Cjwvc3ZnPg==')"
      }
    },
  },
  variants: {
    extend: {
      backgroundImage: ['dark'],
      ringWidth: ['hover', 'dark'],
      brightness: ['hover', 'focus', 'dark', 'group-hover'],
      boxShadow: ['dark'],
      placeholderOpacity: ['dark'],
      backgroundColor: ['group-focus'],
      textColor: ['group-focus'],
      borderWidth: ['dark'],
    }
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
