import Head from 'next/head'
import Layout from '../components/layout'


export default function Home() {
  return (
    <Layout home>
      <Head>
        <title>SPK Network Claim Drop</title>
      </Head>
    </Layout>
  )
}
