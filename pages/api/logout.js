import { withSessionRoute } from "../../lib/withSession";

export default withSessionRoute(logoutRoute);

async function logoutRoute(req, res) {
  let keepKeychainState = req.session.user.haveKeychain
  //console.log('Keychain State?', keepKeychainState)
  req.session.destroy()
  res.json({
    isLoggedIn: false,
    authMethod: "",
    username: "",
    profile: "",
    callStatus: 200,
    haveKeychain: keepKeychainState
  });
}