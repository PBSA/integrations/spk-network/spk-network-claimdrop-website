import dbConnect from '../../../../lib/dbConnect'
import ClaimUser from '../../../../models/ClaimUser'
import CryptoJS, { AES } from 'crypto-js'
import { Client } from '@hiveio/dhive'
import axios from 'axios'
import fetchJson from "../../../../lib/fetchJson"
import absoluteUrl from 'next-absolute-url'
var Decimal = require('decimal.js-light')

export default async (req, res) => {

  // get the query & method...
  const {
    query: { user },
    body,
    method,
  } = req

  let db, session, hive_name, qReturn
  const query = { hive_name: user }

  const { origin } = absoluteUrl(req, "localhost:3000")

  let a = await fetchJson(`${origin}/api/admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({apiVerify: process.env.SESSION_SALT})
  })
  let adminSettings = a.adminSettings
  //console.log('Claims', adminSettings)
  let BLACKLIST = JSON.parse(adminSettings.BLACKLISTED_ACCOUNTS)
  let SPECIAL = JSON.parse(adminSettings.SPECIAL_ACCOUNTS)

  if(BLACKLIST.find(n => user == n) != undefined) {
    res.status(200).json({data: 'Account is blacklisted from the claim drop.', status: 'blacklisted'})
    return
  }

  if(SPECIAL.find(n => user == n) != undefined) {
    res.status(200).json({data: 'Account is a special account and excluded from the claim drop.', status: 'special'})
    return
  }
    
  // check the method...
  switch (method) {
    case 'GET':
      // let peerplaysAccount = null
      // get the db...
      await dbConnect()
      qReturn = await ClaimUser.findOne(query).exec()

      if(qReturn === undefined || qReturn === null || qReturn.peerplays_verified == null || qReturn.peerplays_verified == false) {
        res.status(200).json({data: 'No records found.'})
        return
      }
      else {
        res.status(200).json(qReturn)
        return
      }
      break
    case 'POST':
      // get the db...
      db = await dbConnect()
      session = await db.startSession()

      let snap
      let c = await fetchJson(`${origin}/api/timing/claim-stage`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })
      let claimRound = Math.ceil(c.claimStage / 2) - 1 // 0 is the initial claim drop, numbers 1-11 for months after that.


      //Do some verification here...
      const encryptedSession = body.sessionData1
      const encryptedKey = body.sessionData2
      const salt1 = process.env.SESSION_SALT
      const salt2 = process.env.KEY_SALT

      //step 1: get the timestamp...
      const hash2 = body.authMethod + salt2 + body.username
      const bytes2 = AES.decrypt(encryptedKey, hash2)
      const decryptedKey = bytes2.toString(CryptoJS.enc.Utf8)

      //step 2: use the timestamp to get ciphertext...
      const hash1 = body.authMethod + salt1 + body.username + decryptedKey
      const bytes1 = AES.decrypt(encryptedSession, hash1)
      const decryptedSession = JSON.parse(bytes1.toString(CryptoJS.enc.Utf8))

      hive_name = decryptedSession.username

      const url = adminSettings.API_ENDPOINT_FETCH_CLAIM_BALANCES
      const client = new Client(['https://api.hive.blog', 'https://anyx.io', 'https://api.openhive.network'])

      try {
        const fullProfile = await client.call('bridge', 'get_profile', { 'account': user })
        snap = await axios.get(url, {params: { u: hive_name }})
        snap.data.profile = fullProfile.metadata.profile
        snap = snap.data
      }
      catch (err) {
        res.status(400).json({ err })
        return
      }

      await session.withTransaction( async () => {
      
        if (hive_name == body.username) { //User is now verified! Woo-hoo!
          let l
          if (snap.Larnyx != null) { l = snap.Larnyx }
          if (snap.Larynx != null) { l = snap.Larynx }

          let availableClaimInRound = new Decimal(l).dividedBy(12)

          // get the previous values...
          qReturn = await ClaimUser.findOne({ hive_name: hive_name }).session(session)

          if (qReturn.peerplays_account == null) {
            // Missed a step! gotta have a peerplays account first!
            session.endSession()
            res.status(200).json({
              callStatus: 400,
              data: 'No Peerplays account has been found.'
            })
            return
          }

          if(qReturn === undefined || qReturn === null || qReturn.length == 0) {
            // No Record? Something went wrong...
            session.endSession()
            res.status(200).json({
              callStatus: 400,
              data: 'No Claim drop account has been found.'
            })
            return
          }
          else {
            // Updating an existing record...
            // fill unclaimed rounds
            for (let i = 0; i < claimRound; i++) {
              if ( typeof qReturn.claims[i] === 'undefined' ) {
                qReturn.claims[i] = {
                  claim_timestamp: null,
                  claim_available_in_round: availableClaimInRound.toDecimalPlaces(3).toNumber(),
                  claim_amount_claimed: 0,
                  claim_round: i
                }
                qReturn.unclaimed_total += availableClaimInRound.toDecimalPlaces(3).toNumber()
                qReturn.unclaimed_rounds += 1
              }
            }

            // fill this claimed round
            if ( typeof qReturn.claims[claimRound] === 'undefined' ) {
              qReturn.claims.push({
                claim_timestamp: new Date,
                claim_available_in_round: availableClaimInRound.toDecimalPlaces(3).toNumber(),
                claim_amount_claimed: availableClaimInRound.toDecimalPlaces(3).toNumber(),
                claim_round: claimRound
              })
              qReturn.claimed_total += availableClaimInRound.toDecimalPlaces(3).toNumber()
              qReturn.claimed_rounds += 1
            }
            await qReturn.save()
          }
        }
        else {
          qReturn = [{
            callStatus: 400,
            data: 'Something went terribly wrong!'
          }]
        }
      })
      session.endSession()

      qReturn = await ClaimUser.findOne({ hive_name: hive_name }).exec()

      if(qReturn === undefined || qReturn === null || qReturn.length == 0) {
        res.status(200).json({data: 'No records found.'})
        return
      }
      else {
        res.status(200).json(qReturn)
        return
      }
      break
    default:
      res.setHeader('Allow', ['GET', 'POST'])
      res.status(405).end(`Method ${method} Not Allowed!`)
  }
}