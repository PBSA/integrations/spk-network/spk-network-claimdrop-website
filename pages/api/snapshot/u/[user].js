import axios from 'axios'
import { Client } from '@hiveio/dhive'
import fetchJson from "../../../../lib/fetchJson"
import absoluteUrl from 'next-absolute-url'

export default async (req, res) => {
  // get the query & method...
  const {
    query: { user },
    method,
  } = req

  const { origin } = absoluteUrl(req, "localhost:3000")
  
  let a = await fetchJson(`${origin}/api/admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({apiVerify: process.env.SESSION_SALT})
  })
  let adminSettings = a.adminSettings
  //console.log('Snapshot', a.adminSettings)
  let BLACKLIST = JSON.parse(adminSettings.BLACKLISTED_ACCOUNTS)
  let SPECIAL = JSON.parse(adminSettings.SPECIAL_ACCOUNTS)

  if(BLACKLIST.find(n => user == n) != undefined) {
    res.status(200).json({data: 'Account is blacklisted from the claim drop.', status: 'blacklisted'})
    return
  }

  if(SPECIAL.find(n => user == n) != undefined) {
    res.status(200).json({data: 'Account is a special account and excluded from the claim drop.', status: 'special'})
    return
  }

  const url = adminSettings.API_ENDPOINT_FETCH_CLAIM_BALANCES
  const client = new Client(['https://api.hive.blog', 'https://anyx.io', 'https://api.openhive.network'])

  // check the method...
  switch (method) {
    case 'GET':
      const fullProfile = await client.call('bridge', 'get_profile', { 'account': user })
      await axios
        .get(url, {
          params: {
            u: user
          }
        })
        .then(({ data }) => {
          data.profile = fullProfile.metadata.profile
          //console.log({ data })
          res.status(200).json({ data })
        })
        .catch(({ err }) => {
          //console.log({ err })
          res.status(400).json({ data: 'No records found.' })
        })
      break
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed!`)
  }
}