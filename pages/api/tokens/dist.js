import { Apis, ChainStore, FetchChain, PrivateKey, TransactionBuilder } from 'peerplaysjs-lib'
import dbConnect from '../../../lib/dbConnect'
import fetchJson from '../../../lib/fetchJson'
import ClaimUser from '../../../models/ClaimUser'
import ClaimState from '../../../models/ClaimState'
import stringify from 'safe-stable-stringify'
import absoluteUrl from 'next-absolute-url'
var Decimal = require('decimal.js-light')

export default async (req, res) => {

  // get the query & method...
  const {
    body,
    method,
  } = req

  let db, session1, session2, session3, qReturn, catchUp, cs, claimRound, issuedToDate, unclaimedToDate, unclaimedIssuedToIssuer, verified = false

  const { origin } = absoluteUrl(req, "localhost:3000")
  let a = await fetchJson(`${origin}/api/admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({apiVerify: process.env.SESSION_SALT})
  })
  let adminSettings = a.adminSettings
  let wsConn = adminSettings.API_ENDPOINT_PEERPLAYS_NODE
  let BLACKLIST = JSON.parse(adminSettings.BLACKLISTED_ACCOUNTS)
  let SPECIAL = JSON.parse(adminSettings.SPECIAL_ACCOUNTS)

  let privKey = process.env.LARYNX_ASSET_ISSUER_PRIVATE_ACTIVE_KEY // Asset Issuer Private Active Key
  let larynxAssetId = process.env.LARYNX_ASSET_ID
  let larynxIssuerId = process.env.LARYNX_ASSET_ISSUER_ID
  let pKey = PrivateKey.fromWif(privKey)

  let c = await fetchJson(`${origin}/api/timing/claim-stage`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  })
  cs = await ClaimState.findOneAndUpdate(
    {record_id: 'claim_state'},
    {claim_stage: c.claimStage, claim_stage_date: c.stageDate, claim_round: Math.ceil(c.claimStage / 2)},
    {new: true, upsert: true}
  ).exec()
  
  claimRound = cs.claim_round == null ? 0 : cs.claim_round
  issuedToDate = cs.issued_to_date == null ? 0 : cs.issued_to_date
  unclaimedToDate = cs.unclaimed_to_date == null ? 0 : cs.unclaimed_to_date
  unclaimedIssuedToIssuer = cs.unclaimed_issued_to_issuer == null ? 0 : cs.unclaimed_issued_to_issuer

  console.log(
    '\nCLAIM STATE at %s\nclaim round (month): %d\nissued LARYNX to date: %d\nunclaimed LARYNX to date: %d\nunclaimed LARYNX issued to spk-treasury: %d\n',
    new Date(),
    claimRound,
    issuedToDate,
    unclaimedToDate,
    unclaimedIssuedToIssuer
  )
  

  if (body.apiVerify == process.env.SESSION_SALT) {
    //console.log('API Verified')
    verified = true
  }

  // check the method...
  switch (method) {
    case 'POST':
      if (!verified) { notAnAdmin() }
      else {
        // let peerplaysAccount = null
        // get the db...
        db = await dbConnect()
        

        // Catch up accounts...
        let catchUpToRound = claimRound - 1
        catchUp = await ClaimUser.find({ $expr: { $lt: [ { $sum: ["$claimed_rounds", "$unclaimed_rounds"]}, catchUpToRound]}}).exec()
        
        if (catchUp.length > 0) {
          session1 = await db.startSession()
          session1.startTransaction()
          console.log('Catching up %d accounts with potentially unclaimed tokens.', catchUp.length)
          let id
          for (let i = 0; i < catchUp.length; i++) {
            id = JSON.parse(stringify(catchUp[i]._id))
            
            let accountInCatchUp = await ClaimUser.findOne({ _id: id }).session(session1)
            // fill unclaimed rounds
            for (let j = 0; j < catchUpToRound; j++) {
              if ( typeof accountInCatchUp.claims[j] === 'undefined' ) {
                accountInCatchUp.claims[j] = {
                  claim_timestamp: null,
                  claim_available_in_round: new Decimal(accountInCatchUp.claimable_per_round).toDecimalPlaces(3).toNumber(),
                  claim_amount_claimed: 0,
                  claim_round: j
                }
                accountInCatchUp.unclaimed_total += new Decimal(accountInCatchUp.claimable_per_round).toDecimalPlaces(3).toNumber()
                accountInCatchUp.unclaimed_rounds += 1
              }
            }
            console.log('Account %s updated to current claim round.', accountInCatchUp.hive_name)
            await accountInCatchUp.save()
          }
          await session1.commitTransaction()
          session1.endSession()
        }
        else {
          console.log('All accounts are updated to the latest claim round.')
        }



        // Process Claims....
        qReturn = await ClaimUser.find({"claims.processed": false}).exec()
        
        //console.log('What is here?', stringify(qReturn[0]._id))
        if(qReturn == null || qReturn.length == 0) {
          console.log('No Claimed Tokens to Process.')
        }
        else {
          session2 = await db.startSession()
          session2.startTransaction()
          console.log('Got unprocessed claims for %d accounts.', qReturn.length)
          let id, toProcess, peerAccount, hiveAccount

          for (let i = 0; i < qReturn.length; i++) {
            id = JSON.parse(stringify(qReturn[i]._id))
            
            let accountInProcess = await ClaimUser.findOne({ _id: id }).session(session2)
            cs = await ClaimState.findOne({ record_id: 'claim_state' }).session(session2)
            toProcess = accountInProcess.claims
            peerAccount = accountInProcess.peerplays_account.peerplays_name
            hiveAccount = accountInProcess.hive_name
            console.log('Processing account #%d: %s (%s) with %d claims.', i+1, hiveAccount, peerAccount, toProcess.length)

            if (toProcess.length > 0) {
              let claimedAmt = new Decimal(0)
              let missedAmt = new Decimal(0)

              for (let j = 0; j < toProcess.length; j++) {
                console.log('#%d: %s (%s) claim %d is processed? %s', i+1, hiveAccount, peerAccount, j+1, toProcess[j].processed)
                if (!toProcess[j].processed) {
                  console.log('#%d: %s (%s) claim %d do process...', i+1, hiveAccount, peerAccount, j+1)
                  if (BLACKLIST.find(n => hiveAccount == n) != undefined || SPECIAL.find(n => hiveAccount == n) != undefined) {
                    console.log('#%d: %s (%s) claim %d ACCOUNT BLACKLISTED OR SPECIAL, skipping claim.', i+1, hiveAccount, peerAccount, j+1)
                  }
                  else {
                    claimedAmt = claimedAmt.plus(toProcess[j].claim_amount_claimed)
                    if (toProcess[j].claim_amount_claimed == 0) {
                      missedAmt = missedAmt.plus(toProcess[j].claim_available_in_round)
                    }
                  }
                  accountInProcess.claims[j].processed = true
                  accountInProcess.claims[j].processed_timestamp = new Date()
                }
              }

              cs.issued_to_date = cs.issued_to_date == null ? claimedAmt.toDecimalPlaces(3).toNumber() : new Decimal(cs.issued_to_date).plus(claimedAmt).toDecimalPlaces(3).toNumber()
              cs.unclaimed_to_date = cs.unclaimed_to_date == null ? missedAmt.toDecimalPlaces(3).toNumber() : new Decimal(cs.unclaimed_to_date).plus(missedAmt).toDecimalPlaces(3).toNumber()
              
              if (claimedAmt.greaterThan(0)) {
                console.log('#%d: %s (%s) Amount of %s LARYNX will be issued.', i+1, hiveAccount, peerAccount, claimedAmt.toFixed(3))
              
                // Do the issue here.
                await Apis.instance(wsConn, true).init_promise.then( async () => {

                  await ChainStore.init().then( async () => {

                    await Promise.all([
                      FetchChain('getAccount', peerAccount)
                    ]).then( async (res) => {

                      let tr = new TransactionBuilder()
                      await tr.add_type_operation('asset_issue', {
                        fee: {
                          amount: 0,
                          asset_id: '1.3.0'
                        },
                        issuer: larynxIssuerId,
                        asset_to_issue: {
                          amount: claimedAmt.times(1000).toDecimalPlaces(3).toNumber(), // because of precision = 3
                          asset_id: larynxAssetId
                        },
                        issue_to_account: JSON.parse(stringify(res[0])).id
                      })

                      await tr.set_required_fees().then( async () => {
                        await tr.add_signer(pKey, pKey.toPublicKey().toPublicKeyString())
                        //console.log('serialized transaction:', tr.serialize())
                      })
                      
                      let final = await tr.broadcast().then( async (res) => {
                        return await res
                      })

                      //console.log('FINAL', final)

                      if (final != null && final.length > 0) {
                        await accountInProcess.save()
                        await cs.save()
                        console.log('%s: hive (pp) accounts: %s (%s) requested: %s issued: %s status: %s',
                          new Date(),
                          hiveAccount,
                          peerAccount,
                          claimedAmt.toFixed(3),
                          claimedAmt.toFixed(3),
                          'success'
                        )
                      }
                      else {
                        console.log('%s: hive (pp) accounts: %s (%s) requested: %s issued: %d status: %s',
                          new Date(),
                          hiveAccount,
                          peerAccount,
                          claimedAmt.toFixed(3),
                          0,
                          'failed'
                        )
                      }

                    }).catch((reason) => {
                      console.log('%s: hive (pp) accounts: %s (%s) requested: %d issued: %s status: %s reason: %s',
                        new Date(),
                        hiveAccount,
                        peerAccount,
                        claimedAmt.toFixed(3),
                        0,
                        'failed',
                        'error fetching peerplays account...'
                      )
                      console.log(reason)
                    })
                  }).catch((reason) => {
                    console.log('ChainStore error...')
                    console.log(reason)
                  })
                }).catch((reason) => {
                  console.log('Peerplays API error...')
                  console.log(reason)
                })

              }
              else {
                console.log('#%d: %s (%s) Amount of %s LARYNX... Nothing to Claim.', i+1, hiveAccount, peerAccount, claimedAmt.toFixed(3))
                await accountInProcess.save()
                await cs.save()
              }
            }
          }
          await session2.commitTransaction()
          session2.endSession()
        }

        // Process Unclaimed Tokens?
        if (claimRound >= 13) {
          session3 = await db.startSession()
          session3.startTransaction()
          let unclaimedTokensToProcess = new Decimal(0)
          // Claim Over
          console.log('Processing Unclaimed Tokens.')
          cs = await ClaimState.findOne({ record_id: 'claim_state' }).session(session3)
          unclaimedTokensToProcess = new Decimal(cs.unclaimed_to_date).minus(cs.unclaimed_issued_to_issuer)
          if (unclaimedTokensToProcess.greaterThan(0)) {
            console.log('Issuing %s unclaimed tokens to spk-treasury.', unclaimedTokensToProcess.toFixed(3))

            cs.unclaimed_issued_to_issuer = cs.unclaimed_issued_to_issuer == null ? unclaimedTokensToProcess.toDecimalPlaces(3).toNumber() : new Decimal(cs.unclaimed_issued_to_issuer).plus(unclaimedTokensToProcess).toDecimalPlaces(3).toNumber()

            await Apis.instance(wsConn, true).init_promise.then( async () => {
              let tr = new TransactionBuilder()
              await tr.add_type_operation('asset_issue', {
                fee: {
                  amount: 0,
                  asset_id: '1.3.0'
                },
                issuer: larynxIssuerId,
                asset_to_issue: {
                  amount: unclaimedTokensToProcess.times(1000).toDecimalPlaces(3).toNumber(), // because of precision = 3
                  asset_id: larynxAssetId
                },
                issue_to_account: larynxIssuerId
              })

              await tr.set_required_fees().then( async () => {
                await tr.add_signer(pKey, pKey.toPublicKey().toPublicKeyString())
                //console.log('serialized transaction:', tr.serialize())
              })
              
              let final = await tr.broadcast().then( async (res) => {
                return await res
              })

              //console.log('FINAL', final)

              if (final != null && final.length > 0) {
                await cs.save()
                console.log('%s: successfully issued %s LARYNX to spk-treasury.',
                  new Date(),
                  unclaimedTokensToProcess.toFixed(3)
                )
              }
              else {
                console.log('%s: failed to issue %s LARYNX to spk-treasury.',
                  new Date(),
                  unclaimedTokensToProcess.toFixed(3)
                )
              }
            }).catch((reason) => {
              console.log('Peerplays API error...')
              console.log(reason)
            })

          }
          else {
            console.log('No Unclaimed Tokens to Process.')
          }
          await session3.commitTransaction()
          session3.endSession()
        }
        else {
          console.log('Not Time to Process Unclaimed Tokens, waiting for claim round 13.')
        }

        console.log('\nProcessing Complete!\n')
        await res.status(200).json({ message: 'Processing Complete' })
        return
      }
      break
    default:
      await res.setHeader('Allow', ['POST'])
      await res.status(405).end(`Method ${method} Not Allowed!`)
      return
  }

  function notAnAdmin() {
    res.json({ 
      message: 'Authorization Failed. Check your username and try again.',
      adminKey: 'invalid',
      callStatus: 401
    })
  }
}