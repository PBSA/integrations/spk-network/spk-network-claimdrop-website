export default (req, res) => {
  
  // get the query & method...
  const {
    query: {},
    body,
    method,
  } = req

  let location

  switch (method) {
    case 'POST':

      if(body.location) {
        location = body.location
      }

      try {
      const hivesignerLink = `https://hivesigner.com/login?` +
                             `client_id=` + process.env.APP_FOR_AUTH +
                             `&redirect_uri=` + location +
                             `&scope=login`
      res.status(200).json({ link: hivesignerLink })
      }
      catch(err) {
          res.status(500).json({ message: err.message })
      }
      break
    default:
      res.setHeader('Allow', ['POST'])
      res.status(405).end(`Method ${method} Not Allowed!`)
  }

}