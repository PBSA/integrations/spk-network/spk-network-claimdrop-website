import { withSessionRoute } from '../../lib/withSession'
import { Client } from '@hiveio/dhive'
import { AES } from 'crypto-js'
import * as Hivesigner from 'hivesigner'
import absoluteUrl from 'next-absolute-url'
import fetchJson from "../../lib/fetchJson"

export default withSessionRoute(loginRoute)

async function loginRoute(req, res) {

  let auth = false
  let hsFullProfile = ''
  let status = 'eligible'
  const { authMethod, username, data, keepKeychainState } = await req.body
  const client = new Client(['https://api.hive.blog', 'https://anyx.io', 'https://api.openhive.network'],
    { chainId: 'beeab0de00000000000000000000000000000000000000000000000000000000' }
  )
  const timestamp = new Date().getTime()

  const { origin } = absoluteUrl(req, "localhost:3000")
  
  let a = await fetchJson(`${origin}/api/admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({apiVerify: process.env.SESSION_SALT})
  })
  let adminSettings = a.adminSettings
  let BLACKLIST = JSON.parse(adminSettings.BLACKLISTED_ACCOUNTS)
  let SPECIAL = JSON.parse(adminSettings.SPECIAL_ACCOUNTS)

  if(BLACKLIST.find(n => username == n) != undefined) {
    status = 'blacklisted'
  }

  if(SPECIAL.find(n => username == n) != undefined) {
    status = 'special'
  }

  if(authMethod == 'Hive Posting Key') {
    // Steps 1 - 4: located in authindicator.js so the private key stays local!
    // Step 5: verify the signed transaction. located here so 'auth' can't be reached (hacked) by the client!
    //console.log('data!! ', data) 
    auth = await client.database.verifyAuthority({ stx: data })
  }
  else if(authMethod == 'Hivesigner') {

    const hsClient = new Hivesigner.Client({
      app: process.env.APP_FOR_AUTH,
      accessToken: data
    })

    hsFullProfile = await hsClient.me(function (err, res) {
      //console.log('It worked!')
      //console.log(err, res)
      return res ? res : err
    })

    if(hsFullProfile) {
      //console.log(hsFullProfile)
      auth = true
    }

  }
  else {
    auth = true
  }

  try {
    //console.log('auth? ' + auth);
    if(auth) {
      let user = ''
      const salt1 = process.env.SESSION_SALT
      const salt2 = process.env.KEY_SALT
      const hash1 = authMethod + salt1 + username + timestamp
      const hash2 = authMethod + salt2 + username

      if(authMethod != 'Hivesigner' || hsFullProfile.user_metadata == null) {
        const fullProfile = await client.call('bridge', 'get_profile', { 'account': username })
        //console.log(fullProfile);
        user = {
          isLoggedIn: auth,
          authMethod: authMethod,
          username: username,
          profile: fullProfile.metadata.profile,
          status: status,
          callStatus: 200,
          haveKeychain: keepKeychainState
        }
      }
      else {
        user = {
          isLoggedIn: auth,
          authMethod: authMethod,
          username: username,
          profile: hsFullProfile.user_metadata.profile,
          status: status,
          callStatus: 200,
          haveKeychain: keepKeychainState
        }
      }

      if(status != 'blacklisted' && status != 'special') {
        const encryptedSession = AES.encrypt(JSON.stringify(user), hash1).toString()
        const encryptedKey = AES.encrypt(timestamp.toFixed(), hash2).toString()
        user.sessionData1 = encryptedSession
        user.sessionData2 = encryptedKey
      }
      //console.log(user);
      req.session.user = user
      await req.session.save()
      res.json(user)
    }
    else {
      res.json({ 
        message: 'Authorization Failed. Check the username / posting key and try again.',
        callStatus: 401
      })
    }
  }
  catch(err) {
    res.json({ 
      message: err.message,
      callStatus: 500
    })
  }
}