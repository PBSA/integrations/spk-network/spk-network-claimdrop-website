import { withSessionRoute } from '../../../../lib/withSession'
import dbConnect from '../../../../lib/dbConnect'
import Logs from '../../../../models/Logs'

export default withSessionRoute(logRoute);

async function logRoute(req, res) {

  const { body, method } = req

  await dbConnect()

  let timestamp = Date.now()
  let logs

  switch (method) {
    case 'POST':
      await Logs.create({
        name: body.name,
        timestamp: timestamp,
        action: body.action
      })
      logs = await Logs.find().sort({timestamp: 'desc'}).limit(250).exec()
      res.json({ 
        logs: logs,
        callStatus: 200
      })
      break
    case 'GET':
      logs = await Logs.find().sort({timestamp: 'desc'}).limit(250).exec()
      res.json({ 
        logs: logs,
        callStatus: 200
      })
      break
      case 'DELETE':
        await Logs.deleteMany().exec()
        await Logs.create({
          name: body.name,
          timestamp: timestamp,
          action: 'deleted the logs.'
        })
        logs = await Logs.find().sort({timestamp: 'desc'}).limit(250).exec()
        res.json({ 
          logs: logs,
          callStatus: 200
        })
        break
    default:
      res.setHeader('Allow', ['POST', 'GET', 'DELETE'])
      res.status(405).end(`Method ${method} Not Allowed!`)
  }

}