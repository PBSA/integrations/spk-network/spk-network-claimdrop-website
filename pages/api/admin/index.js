import { withSessionRoute } from '../../../lib/withSession'
import CryptoJS, { AES } from 'crypto-js'
import dbConnect from '../../../lib/dbConnect'
import AdminSettings from '../../../models/AdminSettings'

export default withSessionRoute(adminRoute);

async function adminRoute(req, res) {

  const { body, method } = req
  let ADMINS = JSON.parse(process.env.ADMIN_ACCOUNTS)
  let adminSettings, allAdmins, publicAdminSettings, admin_name
  let verified = false
  //console.log(body)
  //ADMIN_ACCOUNTS=["hiltos1","bobinson"]

  await dbConnect()
  adminSettings = await AdminSettings.findOne({name: 'adminsettings'}).exec()
  //adminSettings = await AdminSettings.deleteOne({ _id: adminSettings._id }).exec()
  
  if (adminSettings == null) {
    //Doesn't exist yet! Load defaults from .env.local!
    await AdminSettings.create({
      name: 'adminsettings',
      ADMIN_ACCOUNTS: process.env.ADMIN_ACCOUNTS,
      BLACKLISTED_ACCOUNTS: process.env.BLACKLISTED_ACCOUNTS,
      SPECIAL_ACCOUNTS: process.env.SPECIAL_ACCOUNTS,
      API_ENDPOINT_FETCH_CLAIM_BALANCES: process.env.API_ENDPOINT_FETCH_CLAIM_BALANCES,
      API_ENDPOINT_PEERPLAYS_NODE: process.env.API_ENDPOINT_PEERPLAYS_NODE,
      API_ENDPOINT_PEERPLAYS_FAUCET: process.env.API_ENDPOINT_PEERPLAYS_FAUCET,
      KEY_PREFIX: process.env.KEY_PREFIX,
      SNAPSHOT_DATETIME: process.env.SNAPSHOT_DATETIME,
      ICD_JAN_START_DATETIME: process.env.ICD_JAN_START_DATETIME,
      ICD_JAN_END_DATETIME: process.env.ICD_JAN_END_DATETIME,
      MCD_FEB_START_DATETIME: process.env.MCD_FEB_START_DATETIME,
      MCD_FEB_END_DATETIME: process.env.MCD_FEB_END_DATETIME,
      MCD_MAR_START_DATETIME: process.env.MCD_MAR_START_DATETIME,
      MCD_MAR_END_DATETIME: process.env.MCD_MAR_END_DATETIME,
      MCD_APR_START_DATETIME: process.env.MCD_APR_START_DATETIME,
      MCD_APR_END_DATETIME: process.env.MCD_APR_END_DATETIME,
      MCD_MAY_START_DATETIME: process.env.MCD_MAY_START_DATETIME,
      MCD_MAY_END_DATETIME: process.env.MCD_MAY_END_DATETIME,
      MCD_JUN_START_DATETIME: process.env.MCD_JUN_START_DATETIME,
      MCD_JUN_END_DATETIME: process.env.MCD_JUN_END_DATETIME,
      MCD_JUL_START_DATETIME: process.env.MCD_JUL_START_DATETIME,
      MCD_JUL_END_DATETIME: process.env.MCD_JUL_END_DATETIME,
      MCD_AUG_START_DATETIME: process.env.MCD_AUG_START_DATETIME,
      MCD_AUG_END_DATETIME: process.env.MCD_AUG_END_DATETIME,
      MCD_SEP_START_DATETIME: process.env.MCD_SEP_START_DATETIME,
      MCD_SEP_END_DATETIME: process.env.MCD_SEP_END_DATETIME,
      MCD_OCT_START_DATETIME: process.env.MCD_OCT_START_DATETIME,
      MCD_OCT_END_DATETIME: process.env.MCD_OCT_END_DATETIME,
      MCD_NOV_START_DATETIME: process.env.MCD_NOV_START_DATETIME,
      MCD_NOV_END_DATETIME: process.env.MCD_NOV_END_DATETIME,
      MCD_DEC_START_DATETIME: process.env.MCD_DEC_START_DATETIME,
      MCD_DEC_END_DATETIME: process.env.MCD_DEC_END_DATETIME
    })
    adminSettings = await AdminSettings.findOne({name: 'adminsettings'}).exec()
  }

  ADMINS = [...ADMINS, ...adminSettings.ADMIN_ACCOUNTS]
  allAdmins = [...new Set(ADMINS)]

  //console.log(adminSettings)
  if (body.session != null) {
    //console.log('session not null')
    if(allAdmins.find(n => body.session.username == n) != undefined) {
      // User is an admin!
      // Do some verification here...
      const encryptedSession = body.session.sessionData1
      const encryptedKey = body.session.sessionData2
      const salt1 = process.env.SESSION_SALT
      const salt2 = process.env.KEY_SALT

      //step 1: get the timestamp...
      const hash2 = body.session.authMethod + salt2 + body.session.username
      const bytes2 = AES.decrypt(encryptedKey, hash2)
      const decryptedKey = bytes2.toString(CryptoJS.enc.Utf8)

      //step 2: use the timestamp to get ciphertext...
      const hash1 = body.session.authMethod + salt1 + body.session.username + decryptedKey
      const bytes1 = AES.decrypt(encryptedSession, hash1)
      const decryptedSession = JSON.parse(bytes1.toString(CryptoJS.enc.Utf8))

      admin_name = decryptedSession.username
      
      if (admin_name == body.session.username) {
        // Admin verified!
        verified = true
      }
      else {
        //console.log('Not admin 1')
        notAnAdmin()
        return
      }
    }
    else {
      //console.log('Not Admin 2')
      notAnAdmin()
      return
    }
  }

  if (body.apiVerify == process.env.SESSION_SALT) {
    //console.log('API Verified')
    verified = true
  }

  // check the method...
  switch (method) {
    case 'GET':
      publicAdminSettings = await AdminSettings
        .findOne({name: 'adminsettings'})
        .select('-_id -__v -name -ADMIN_ACCOUNTS -BLACKLISTED_ACCOUNTS -SPECIAL_ACCOUNTS -API_ENDPOINT_FETCH_CLAIM_BALANCES -API_ENDPOINT_PEERPLAYS_NODE -API_ENDPOINT_PEERPLAYS_FAUCET -KEY_PREFIX')
        .exec()
      res.json({ 
        message: 'Getting settings.',
        adminKey: 'invalid',
        adminSettings: publicAdminSettings,
        callStatus: 200
      })
      return
      break
    case 'POST':
      if (!verified) { notAnAdmin() }
      else {
        // User is an admin!
        //console.log('Admins', adminSettings)
        res.json({ 
          message: 'Welcome, Admin ' + admin_name + '!',
          adminKey: 'valid',
          adminSettings: adminSettings,
          callStatus: 200
        })
        return
      }
      break
    case 'PUT':
      if (!verified) { notAnAdmin() }
      else {
        for (let key in body) {
          if (key != 'name' && key != '_id' && key != 'session') {
            if (key == 'ADMIN_ACCOUNTS' || key == 'BLACKLISTED_ACCOUNTS' || key == 'SPECIAL_ACCOUNTS') {
              let a, b
              a = JSON.parse(adminSettings[key])
              b = JSON.parse(body[key])
              let tempArr = [...a, ...b]
              tempArr = [...new Set(tempArr)]
              adminSettings[key] = JSON.stringify(tempArr)
            }
            else {
              adminSettings[key] = body[key]
            }
          }
        }
        await adminSettings.save()
        res.json({ 
          message: 'An admin setting has been updated to a new value.',
          adminSettings: adminSettings,
          callStatus: 200
        })
        return
      }
      break
    case 'DELETE':
      if (!verified) { notAnAdmin() }
      else {
        for (let key in body) {
          if (key != 'session') {
            if (key == 'ALL') {
              await AdminSettings.deleteOne({ _id: adminSettings._id }).exec()
              await AdminSettings.create({
                name: 'adminsettings',
                ADMIN_ACCOUNTS: process.env.ADMIN_ACCOUNTS,
                BLACKLISTED_ACCOUNTS: process.env.BLACKLISTED_ACCOUNTS,
                SPECIAL_ACCOUNTS: process.env.SPECIAL_ACCOUNTS,
                API_ENDPOINT_FETCH_CLAIM_BALANCES: process.env.API_ENDPOINT_FETCH_CLAIM_BALANCES,
                API_ENDPOINT_PEERPLAYS_NODE: process.env.API_ENDPOINT_PEERPLAYS_NODE,
                API_ENDPOINT_PEERPLAYS_FAUCET: process.env.API_ENDPOINT_PEERPLAYS_FAUCET,
                KEY_PREFIX: process.env.KEY_PREFIX,
                SNAPSHOT_DATETIME: process.env.SNAPSHOT_DATETIME,
                ICD_JAN_START_DATETIME: process.env.ICD_JAN_START_DATETIME,
                ICD_JAN_END_DATETIME: process.env.ICD_JAN_END_DATETIME,
                MCD_FEB_START_DATETIME: process.env.MCD_FEB_START_DATETIME,
                MCD_FEB_END_DATETIME: process.env.MCD_FEB_END_DATETIME,
                MCD_MAR_START_DATETIME: process.env.MCD_MAR_START_DATETIME,
                MCD_MAR_END_DATETIME: process.env.MCD_MAR_END_DATETIME,
                MCD_APR_START_DATETIME: process.env.MCD_APR_START_DATETIME,
                MCD_APR_END_DATETIME: process.env.MCD_APR_END_DATETIME,
                MCD_MAY_START_DATETIME: process.env.MCD_MAY_START_DATETIME,
                MCD_MAY_END_DATETIME: process.env.MCD_MAY_END_DATETIME,
                MCD_JUN_START_DATETIME: process.env.MCD_JUN_START_DATETIME,
                MCD_JUN_END_DATETIME: process.env.MCD_JUN_END_DATETIME,
                MCD_JUL_START_DATETIME: process.env.MCD_JUL_START_DATETIME,
                MCD_JUL_END_DATETIME: process.env.MCD_JUL_END_DATETIME,
                MCD_AUG_START_DATETIME: process.env.MCD_AUG_START_DATETIME,
                MCD_AUG_END_DATETIME: process.env.MCD_AUG_END_DATETIME,
                MCD_SEP_START_DATETIME: process.env.MCD_SEP_START_DATETIME,
                MCD_SEP_END_DATETIME: process.env.MCD_SEP_END_DATETIME,
                MCD_OCT_START_DATETIME: process.env.MCD_OCT_START_DATETIME,
                MCD_OCT_END_DATETIME: process.env.MCD_OCT_END_DATETIME,
                MCD_NOV_START_DATETIME: process.env.MCD_NOV_START_DATETIME,
                MCD_NOV_END_DATETIME: process.env.MCD_NOV_END_DATETIME,
                MCD_DEC_START_DATETIME: process.env.MCD_DEC_START_DATETIME,
                MCD_DEC_END_DATETIME: process.env.MCD_DEC_END_DATETIME
              })
              adminSettings = await AdminSettings.findOne({name: 'adminsettings'}).exec()
              res.json({ 
                message: 'All admin settings have been reset to their default values.',
                adminSettings: adminSettings,
                callStatus: 200
              })
              return
            }
            else {
              adminSettings = await AdminSettings.findOneAndUpdate({ name: 'adminsettings' }, { [key]: body[key] }, { new: true })
              res.json({ 
                message: 'One admin setting was reset to its default value.',
                adminSettings: adminSettings,
                callStatus: 200
              })
              return
            }
          }
        }
      }
      
      break
    default:
      res.setHeader('Allow', ['GET','POST', 'PUT', 'DELETE'])
      res.status(405).end(`Method ${method} Not Allowed!`)
      return
  }

  function notAnAdmin() {
    res.json({ 
      message: 'Authorization Failed. Check your username and try again.',
      adminKey: 'invalid',
      callStatus: 401
    })
  }
}