import fetchJson from "../../../lib/fetchJson"
import absoluteUrl from 'next-absolute-url'
import { zonedTimeToUtc  } from 'date-fns-tz'

export default async (req, res) => {
  // get the method...
  const { method } = req
  
  // check the method...
  switch (method) {
    case 'GET':
      const { origin } = absoluteUrl(req, "localhost:3000")

      let a = await fetchJson(`${origin}/api/admin`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })
      let adminSettings = a.adminSettings
      let claimStage = 0 // 0 = countdown to snapshot, 1 = countdown to ICD start, 2 = countdown to ICD end... etc.

      // from Object to an Array:
      const keys = Object.keys(adminSettings)
      const dateArray = []
      const keyArray = []
      for(let i = 0; i < keys.length; i++){
        dateArray.push(adminSettings[keys[i]])
        keyArray.push(keys[i])
      }

      dateArray.sort((a, b) => new Date(a).getTime() - new Date(b).getTime())

      let now = zonedTimeToUtc(new Date(), 'America/Los_Angeles').getTime()
      //console.log('now', now)
      dateArray.forEach(element => {
        let check = zonedTimeToUtc(new Date(element), 'America/Los_Angeles').getTime()
        //console.log('check', check)
        if (check <= now) { claimStage += 1 }
        //console.log('eval', check <= now)
      })

      res.status(200).json({ claimStage, stageDate: dateArray[claimStage], allStageDates: dateArray, allStageDescriptions: [
        'Hive Snapshot',
        'Round 1 Claim Drop Starts',
        'Round 1 Claim Drop Ends',
        'Round 2 Claim Drop Starts',
        'Round 2 Claim Drop Ends',
        'Round 3 Claim Drop Starts',
        'Round 3 Claim Drop Ends',
        'Round 4 Claim Drop Starts',
        'Round 4 Claim Drop Ends',
        'Round 5 Claim Drop Starts',
        'Round 5 Claim Drop Ends',
        'Round 6 Claim Drop Starts',
        'Round 6 Claim Drop Ends',
        'Round 7 Claim Drop Starts',
        'Round 7 Claim Drop Ends',
        'Round 8 Claim Drop Starts',
        'Round 8 Claim Drop Ends',
        'Round 9 Claim Drop Starts',
        'Round 9 Claim Drop Ends',
        'Round 10 Claim Drop Starts',
        'Round 10 Claim Drop Ends',
        'Round 11 Claim Drop Starts',
        'Round 11 Claim Drop Ends',
        'Round 12 Claim Drop Starts',
        'Round 12 Claim Drop Ends',
        'Claim Drop Over'
      ] })

      break
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed!`)
  }
}