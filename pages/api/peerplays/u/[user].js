import { Login, ChainStore, FetchChain, Apis, PrivateKey, key } from 'peerplaysjs-lib'
import stringify from 'safe-stable-stringify'
import dbConnect from '../../../../lib/dbConnect'
import ClaimUser from '../../../../models/ClaimUser'
import fetchJson from '../../../../lib/fetchJson'
import axios from 'axios'
import absoluteUrl from 'next-absolute-url'
var Decimal = require('decimal.js-light')

export default async (req, res) => {
  // get the query & method...
  const {
    query: { user },
    body,
    method,
  } = req

  const { origin } = absoluteUrl(req, "localhost:3000")

  let a = await fetchJson(`${origin}/api/admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({apiVerify: process.env.SESSION_SALT})
  })
  let adminSettings = a.adminSettings
  //console.log('Peerplays', adminSettings)
  let wsConn = adminSettings.API_ENDPOINT_PEERPLAYS_NODE
  let faucetURL = adminSettings.API_ENDPOINT_PEERPLAYS_FAUCET
  let keyPrefix = adminSettings.KEY_PREFIX

  let db, session, qReturn, snap, availableClaimInRound
  const url = adminSettings.API_ENDPOINT_FETCH_CLAIM_BALANCES
  let claimRound = 0 // 0 is the initial claim drop, numbers 1-11 for months after that.

  // check the method...
  switch (method) {
    case 'GET':
      // Pull existing account...
      await Apis.instance(wsConn, true, 5000).init_promise.then( async (result) => {
        //console.log('connected to:', JSON.parse(stringify(result)))
        //console.log('result:', result)
        //res.status(200).json({ result })
        await ChainStore.init().then( async () => {
          let lookupAccount = user

          await Promise.all([
            FetchChain('getAccount', lookupAccount, 5000)
          ]).then((result) => {
            //console.log(result[0])
            let data = JSON.parse(stringify(result[0]))
            res.status(200).json({ data })
            return
          }).catch((reason) => {
            //console.log(reason)
            res.status(200).json({ data: 'No records found.' })
            return
          })// FetchChain
        }) // ChainStore
      }) // API connection
      break
    case 'PUT':
      // Create a new account...
      // One last validation check on the inputs...
      if (!body.username.match(/^[a-z](?!.*([-.])\1)((?=.*(-))|(?=.*([0-9])))[a-z0-9-.]{2,62}(?<![-.])$/g)) {
        res.status(200).json({
          created: false,
          data: 'Username is malformed.'
        })
        return
      }
      if (!body.pass.length >= 12) {
        res.status(200).json({
          created: false,
          data: 'Password is too short.'
        })
        return
      }

      let usernameTaken = false
      await Apis.instance(wsConn, true, 5000).init_promise.then( async () => {
        await ChainStore.init().then( async () => {
          let lookupAccount = body.username
          let lookupPass = body.pass

          await Promise.all([
            FetchChain('getAccount', lookupAccount, 5000)
          ]).then((result) => {
            let data = JSON.parse(stringify(result[0]))
            //console.log(data)
            if(lookupAccount == data.name) { 
              usernameTaken = true
              res.status(200).json({
                usernameTaken: true
              })
              return
            }
          }).catch( async (reason) => {
            console.log(reason)
            // Time Out = Not Found
          })// FetchChain 'getAccount'

          // Username not found on the chain
          if (usernameTaken == false) {

            let roles = ['owner', 'active', 'memo']
            let keys = Login.generateKeys(lookupAccount, lookupPass, roles, keyPrefix)
            let returnKeys = JSON.parse(stringify(keys))
            
            let ownerPublic = returnKeys.pubKeys.owner
            let activePublic = returnKeys.pubKeys.active
            let memoPublic = returnKeys.pubKeys.memo

            //let activePrivateWif = PrivateKey.fromSeed( key.normalize_brainKey(lookupPass + lookupAccount + 'active') )
            //console.log('Made it to 3', activePrivateWif.toWif())
            //let activePrivateKey = PrivateKey.fromWif(activePrivateWif.toWif())

            console.log(lookupAccount)
            console.log(ownerPublic)
            console.log(activePublic)
            console.log(memoPublic)

            let bodyForFaucet = {
              account: {
                name: lookupAccount,
                owner_key: ownerPublic,
                active_key: activePublic,
                memo_key: memoPublic
              }
            }

            let p = await fetchJson(faucetURL, {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify(bodyForFaucet)
            })

            // Should be a success! Add the Peerplays account to the claim data...
            if (p.account != null) {
              // Add peerplays info to a new claim record
              snap = await axios.get(url, {params: { u: user }})
              snap = snap.data

              let l
              if (snap.Larnyx != null) { l = snap.Larnyx }
              if (snap.Larynx != null) { l = snap.Larynx }

              // get the db...
              db = await dbConnect()
              session = await db.startSession()

              await session.withTransaction( async () => {

                availableClaimInRound = new Decimal(l).dividedBy(12)

                // get the previous values...
                qReturn = await ClaimUser.findOne({ hive_name: user }).session(session)
                if (qReturn === undefined || qReturn === null || qReturn.length == 0) {
                  // New Record!
                  qReturn = await ClaimUser.create([{ hive_name: user }], { session: session })
                  qReturn = await ClaimUser.findOne({ hive_name: user }).session(session)

                  qReturn.claimable_total = l
                  qReturn.claimable_per_round = availableClaimInRound.toDecimalPlaces(3).toNumber()
                  qReturn.unclaimed_total = 0
                  qReturn.claimed_total = 0
                  qReturn.unclaimed_rounds = 0
                  qReturn.claimed_rounds = 0
                  
                  // fill unclaimed rounds
                  for (let i = 0; i < claimRound; i++) {
                    qReturn.claims.push({
                      claim_timestamp: null,
                      claim_available_in_round: availableClaimInRound.toDecimalPlaces(3).toNumber(),
                      claim_amount_claimed: 0,
                      claim_round: i
                    })
                    qReturn.unclaimed_total += availableClaimInRound.toDecimalPlaces(3).toNumber()
                    qReturn.unclaimed_rounds += 1
                  }

                  qReturn.peerplays_account = {
                    peerplays_name: p.account.name,
                    creation_timestamp: new Date,
                    owner_pubkey: p.account.owner_key,
                    active_pubkey: p.account.active_key,
                    memo_pubkey: p.account.memo_key
                  }
                  qReturn.peerplays_verified = true
                  qReturn.peerplays_larynx = 0

                  await qReturn.save()
                }
              })
              session.endSession()

              qReturn = await ClaimUser.findOne({ hive_name: user }).exec()

              if(qReturn === undefined || qReturn === null || qReturn.length == 0) {
                res.status(200).json({
                  data: 'No records found.'
                })
                return
              }
              else {
                res.status(200).json(qReturn)
                return
              }
            }
            else {
              res.status(200).json({ 
                data: p,
                message: 'Something went wrong.'
              })
              return
            }
            /*
            let tr = new TransactionBuilder()
            tr.add_type_operation('account_create', {
              'fee': {
                'amount': 0,
                'asset_id': 0
              },
              'registrar': '',
              'referrer': '',
              'referrer_percent': 0,
              'name': lookupAccount,
              'owner': {
                'weight_threshold': 1,
                'account_auths': [],
                'key_auths': [
                  [ownerPublic, 1]
                ],
                'address_auths': []
              },
              'active': {
                'weight_threshold': 1,
                'account_auths': [],
                'key_auths': [
                  [activePublic, 1]
                ],
                'address_auths': []
              },
              'options': {
                'memo_key': memoPublic,
                'voting_account': '1.2.5',
                'num_witness': 0,
                'num_committee': 0,
                'votes': []
              }
            })

            await tr.set_required_fees().then(() => {
              tr.add_signer(activePrivateKey, activePrivateKey.toPublicKey().toPublicKeyString())
              console.log('serialized transaction:', tr.serialize())
              tr.broadcast()
              res.status(200).json({ 
                data: 'Transaction Successful',
                tr: tr.serialize(),
                created: true
              })
              return
            }).catch((reason) => {
              //console.log(reason)
              res.status(200).json({ 
                data: 'No records found.',
                created: false,
                message: reason
              })
              return
            })
            */

          }

        }) // ChainStore
      }) // API connection

      break
    case 'POST':
      // Verify an existing account...
      await Apis.instance(wsConn, true, 5000).init_promise.then(async () => {
        let lookupAccount = body.username
        let lookupPass = body.pass

        await ChainStore.init().then(async () => {
          await Promise.all([
            FetchChain('getAccount', lookupAccount, 5000)
          ]).then( async (result) => {

            let roles = ['owner', 'active', 'memo']
            let keys = Login.generateKeys(lookupAccount, lookupPass, roles, 'PPY')
            let returnKeys = JSON.parse(stringify(keys))

            //let pkey = PrivateKey.fromSeed( key.normalize_brainKey(lookupPass + lookupAccount + 'active') );
            //console.log("Private key  :", pkey.toWif());
            //console.log("Public key   :", pkey.toPublicKey().toString());
            
            let data = JSON.parse(stringify(result[0]))
            let ownerAuth = data.owner.key_auths[0]
            let activeAuth = data.active.key_auths[0]
            let memoAuth = data.options.memo_key

            //console.log("OwnerPub key :", returnKeys.pubKeys.owner)
            //console.log(ownerAuth[0])
            //console.log("ActivePub key:", returnKeys.pubKeys.active)
            //console.log(activeAuth[0])

            if (returnKeys.pubKeys.owner == ownerAuth[0] && returnKeys.pubKeys.active == activeAuth[0]) {
              // Account is verified!
              // Add peerplays info to a new claim record
              snap = await axios.get(url, {params: { u: user }})
              snap = snap.data
              let l
              if (snap.Larnyx != null) { l = snap.Larnyx }
              if (snap.Larynx != null) { l = snap.Larynx }

              // get the db...
              db = await dbConnect()
              session = await db.startSession()

              await session.withTransaction( async () => {

                availableClaimInRound = new Decimal(l).dividedBy(12)
                // get the previous values...
                qReturn = await ClaimUser.findOne({ hive_name: user }).session(session)

                if (qReturn === undefined || qReturn === null || qReturn.length == 0) {
                  // New Record!
                  qReturn = await ClaimUser.create([{ hive_name: user }], { session: session })
                  qReturn = await ClaimUser.findOne({ hive_name: user }).session(session)

                  qReturn.claimable_total = l
                  qReturn.claimable_per_round = availableClaimInRound.toDecimalPlaces(3).toNumber()
                  qReturn.unclaimed_total = 0
                  qReturn.claimed_total = 0
                  qReturn.unclaimed_rounds = 0
                  qReturn.claimed_rounds = 0
                  
                  // fill unclaimed rounds
                  for (let i = 0; i < claimRound; i++) {
                    qReturn.claims.push({
                      claim_timestamp: null,
                      claim_available_in_round: availableClaimInRound.toDecimalPlaces(3).toNumber(),
                      claim_amount_claimed: 0,
                      claim_round: i
                    })
                    qReturn.unclaimed_total += availableClaimInRound.toDecimalPlaces(3).toNumber()
                    qReturn.unclaimed_rounds += 1
                  }

                  qReturn.peerplays_account = {
                    peerplays_name: lookupAccount,
                    creation_timestamp: new Date,
                    owner_pubkey: ownerAuth[0],
                    active_pubkey: activeAuth[0],
                    memo_pubkey: memoAuth
                  }
                  qReturn.peerplays_verified = true
                  qReturn.peerplays_larynx = 0

                  await qReturn.save()
                }
              })
              session.endSession()

              qReturn = await ClaimUser.findOne({ hive_name: user }).exec()

              if(qReturn === undefined || qReturn === null || qReturn.length == 0) {
                //console.log('not found?')
                res.status(200).json({
                  data: 'No records found.'
                })
                return
              }
              else {
                //console.log('found?')
                res.status(200).json(qReturn)
                return
              }
            }
            else {
              //console.log('Dont know?')
              res.status(200).json({ 
                data
              })
              return
            }
          }).catch((reason) => {
            console.log(reason)
            res.status(200).json({ 
              data: 'No records found.',
              message: reason
            })
            return
          })// FetchChain
        }) // ChainStore
      }) // API connection

      break
    default:
      res.setHeader('Allow', ['GET','PUT','POST'])
      res.status(405).end(`Method ${method} Not Allowed!`)
  }
}