import { useUserContextState } from '../../context/usercontext'
import Head from 'next/head'
import { useLayoutEffect } from 'react'
import ClaimWithUser from '../../components/claim/claimWithUser'
import ClaimWithoutUser from '../../components/claim/claimWithoutUser'
import Layout from '../../components/layout'
import { useRouter } from 'next/router'
import { withSessionSsr } from '../../lib/withSession'
import absoluteUrl from 'next-absolute-url'

export default function Claim({ origin }) {
  const router = useRouter()
  const { user } = useUserContextState()

  useLayoutEffect(() => {
    if (user != null && user.isLoggedIn) {
      router.push('/claim/' + user.username)
    }
  }, [user])

  return (
    <Layout>
      <Head>
        <title>SPK Network Claim Drop - Claim LARYNX</title>
      </Head>
      {user == null || !user.isLoggedIn ? <ClaimWithoutUser origin={origin} /> : <ClaimWithUser origin={origin} />}
    </Layout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({ req }) {
    const user = req.session.user

    const { origin } = absoluteUrl(req, "localhost:3000")

    if (user != null && user.isLoggedIn) {
      return {
        redirect: {
          destination: '/claim/' + user.username,
          permanent: false
        }
      }
    }

    return { props: { origin } }
  }
)
