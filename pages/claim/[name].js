import Layout from '../../components/layout'
import Head from 'next/head'
import { useUserContextState } from '../../context/usercontext'
import { useRouter } from 'next/router'
import ClaimWithUser from '../../components/claim/claimWithUser'
import ClaimWithoutUser from '../../components/claim/claimWithoutUser'
import { useLayoutEffect } from 'react'
import { withSessionSsr } from '../../lib/withSession'
import absoluteUrl from 'next-absolute-url'

export default function Claim({ origin }) {
  const router = useRouter()
  const { user } = useUserContextState()

  useLayoutEffect(() => {
    if (user == null || !user.isLoggedIn) {
      router.push('/claim')
    }
  }, [user])

  return (
    <Layout>
      <Head>
        <title>SPK Network Claim Drop - Claim LARYNX</title>
      </Head>
      {user != null && user.isLoggedIn ? <ClaimWithUser origin={origin} /> : <ClaimWithoutUser origin={origin} />}
    </Layout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({ req }) {
    const user = req.session.user

    const { origin } = absoluteUrl(req, "localhost:3000")

    if (user == null || !user.isLoggedIn) {
      return {
        redirect: {
          destination: '/claim',
          permanent: false
        }
      }
    }

    return { props: { origin } }
  }
)