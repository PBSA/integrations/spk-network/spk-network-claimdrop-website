import '../styles/global.css'
import { ThemeProvider } from 'next-themes'
import { config, library } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { Toaster } from 'react-hot-toast'
import { UserContextProvider } from '../context/usercontext'
import { SWRConfig } from 'swr'
import fetchJson from '../lib/fetchJson'
import ClientOnly from '../lib/clientOnly'

config.autoAddCss = false
library.add(fab, far, fas)

export default function App({ Component, pageProps }) {

  let toastOptions = {
    style: {
      borderRadius: '0.25rem',
      border: '1px solid #0148BE',
      background: '#F0F0F8',
      color: '#212529',
    },
    success: {
      style: {
        borderRadius: '0.25rem',
        border: '1px solid #2ADF5D',
        background: '#F0F0F8',
        color: '#212529',
      },
    },
    error: {
      style: {
        borderRadius: '0.25rem',
        border: '1px solid #E31337',
        background: '#F0F0F8',
        color: '#212529',
      },
    },
  }

  return (
    <SWRConfig
      value={{
        fetcher: fetchJson,
        onError: (err) => {
          console.error(err);
        },
      }}
    >
      <UserContextProvider>
        <ThemeProvider attribute="class">
          <ClientOnly>
            <Toaster toastOptions={toastOptions} position='bottom-center'/>
            <Component {...pageProps} />
          </ClientOnly>
        </ThemeProvider>
      </UserContextProvider>
    </SWRConfig>
  );
}
