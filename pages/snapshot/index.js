import { useRef, useLayoutEffect } from 'react'
import useState from 'react-usestateref'
import Layout from '../../components/layout'
import Head from 'next/head'
import { withSessionSsr } from '../../lib/withSession'
import fetchJson from '../../lib/fetchJson'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import { useUserContextState } from '../../context/usercontext'
import absoluteUrl from 'next-absolute-url'
var Decimal = require('decimal.js-light')

export default function Snapshot({ origin }) {

  const { user } = useUserContextState()

  const [initialLoad, setInitialLoad] = useState(true)
  const [tempSearch, setTempSearch] = useState('')
  const [avatarLoaded, setAvatarLoaded] = useState(false)
  const [currentSnapPage, setCurrentSnapPage] = useState(null)
  const [claimHistory, setClaimHistory] = useState(null)
  const [doneLoadingSearch, setDoneLoadingSearch] = useState(true)
  const [userPeerData, setUserPeerData, userPeerDataRef] = useState(null)
  const searchRef = useRef('')
  const runningRef = useRef(false)

  useLayoutEffect(() => {
    if(!runningRef.current){
      if (user != null && user.username != null) { 
        setTempSearch(user.username)
        getSnapPage(user.username)
      }
    }
  }, [user])

  async function getSnapPage( textSearch ) {
    runningRef.current = true
    setAvatarLoaded(false)
    if ( textSearch == '' ) { 
      searchRef.current = ''
      setInitialLoad(true)
      setCurrentSnapPage(null)
      setClaimHistory(null)
      setUserPeerData(null)
      setDoneLoadingSearch(true)
      runningRef.current = false
      return
    }
    searchRef.current = textSearch
    setInitialLoad(false)
    setDoneLoadingSearch(false)

    try {
      let snap, claims, peer = null
      snap = await fetchJson(`${origin}/api/snapshot/u/${textSearch}`, {
        method: 'GET'
      })
      console.log('snap', snap)
      claims = await fetchJson(`${origin}/api/claim/u/${textSearch}`, {
        method: 'GET'
      })
      console.log('claims', claims)
      if (claims != null && claims.peerplays_account?.peerplays_name != null) {
        peer = await fetchJson(`${origin}/api/peerplays/u/${claims.peerplays_account.peerplays_name}`, {
          method: 'GET'
        })
        console.log('peer', peer)
      }

      setCurrentSnapPage(snap)
      setClaimHistory(claims)
      setUserPeerData(peer)
      setDoneLoadingSearch(true)
      runningRef.current = false
    }
    catch(err) {
      let snap = { data: 'invalid' }
      setCurrentSnapPage(snap)
      setDoneLoadingSearch(true)
      runningRef.current = false
      //console.log(err.message)
    }
  }

  function clearSearch() {
    setTempSearch('')
    getSnapPage('')
  }

  function onKeyDownSubmit(event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      document.getElementById('search-btn').click()
    }
  }

  function renderLoading() {
    return (
      <div className={`flex flex-grow justify-center items-center w-full h-32 animate-pulse`}>
        <FontAwesomeIcon icon={['fas', 'stopwatch']} className={`animate-bounce mr-2`} />
        Loading...
      </div>
    )
  }

  function renderSearchPrompt() {
    return (
      <div className={`flex flex-col lg:flex-row flex-grow justify-center items-center w-full h-32 whitespace-nowrap`}>
        <FontAwesomeIcon icon={['fas', 'search']} className={`lg:mr-2`} />
        <div className={`mt-2 lg:mt-1 text-center`}>Enter a Hive account username&nbsp;</div>
        <div className={`lg:mt-1 text-center`}>to lookup claim drop information.</div>
      </div>
    )
  }

  function renderDisplay(snapPageData, prevClaimData, peerData) {
    if (initialLoad) { return ('') }
    if (snapPageData == 'invalid') { return (renderNotFound()) }
    let username, currentLarynx, canClaimNow, isLoggedUser, profilePic, havePeerData
    let claimableLarynx
    isLoggedUser = false
    canClaimNow = false
    havePeerData = false
    username = snapPageData.username
    if (snapPageData.Larynx != null) { currentLarynx = snapPageData.Larynx }
    if (snapPageData.Larnyx != null) { currentLarynx = snapPageData.Larnyx }
    claimableLarynx = currentLarynx
    profilePic = snapPageData.profile.profile_image

    if (currentLarynx > 0) { canClaimNow = true }
    if (username == user.username) { isLoggedUser = true }
    if (peerData != null) { havePeerData = true }

    if (prevClaimData != null && prevClaimData.claims != null && prevClaimData.claims.length > 0 ) {
      claimableLarynx = prevClaimData.claimable_total
    }

    return(
      <div className={`flex-initial w-full mb-4`}>
        <div className={`flex justify-center font-mono w-full`}>

          <div className={`flex flex-col p-4 card`}>
            <h4 className={`flex self-center`}>
              {username}
            </h4>
            <h6 className={`mt-3 mb-1 uppercase`}>Account Details</h6>
            <div className={`flex flex-col`}>
              <p>Hive</p>
              <div className={`flex flex-row min-h-96px`}>
                <div className={`flex my-auto mx-4`}>
                  {renderAvatar(profilePic)}
                </div>
                <div className={`flex flex-col`}>
                  <p>Hive Account: {username}</p>
                  <p>Claimable LARYNX: {new Decimal(claimableLarynx).toFixed(3)}</p>
                  <p>({new Decimal(claimableLarynx).dividedBy(12).toFixed(3)} per monthly drop)</p>
                  {canClaimNow && isLoggedUser ? (<p><Link href={`/claim/` + username}><a className={`link link-underline`}>Go To Claim Page</a></Link></p>) : ``}
                </div>
              </div>
              {havePeerData ? (
              <>
              <p>Peerplays</p>
              <div className={`flex flex-row min-h-96px`}>
                <div className={`flex my-auto mx-4`}>
                  {renderAvatar(`/images/peerplays-logo.svg`)}
                </div>
                <div className={`flex flex-col`}>
                  <p>Peerplays Account: {peerData.data.name}</p>
                </div>
              </div>
              </>
              ) : (<>
                <p className={`mb-2`}>Peerplays</p>
                <div className={`flex flex-row min-h-96px`}>
                <div className={`flex my-auto mx-4`}>
                  {renderAvatar(`/images/peerplays-logo.svg`)}
                </div>
                <div className={`flex flex-col`}>
                  <p>Peerplays Account:</p>
                  <p>not registered</p>
                </div>
              </div>
              </>)}
            </div>
            <h6 className={`mt-3 mb-1 uppercase`}>Claim History</h6>
            { renderClaimHistory(prevClaimData) }
            { renderSupportLink() }
          </div>

        </div>
      </div>
    )
  }

  function renderNotFound() {
    return (
      <>
      <div className={`flex flex-col 2xl:flex-row flex-grow justify-center items-center w-full h-32 md:whitespace-nowrap`}>
        <FontAwesomeIcon icon={['fas', 'exclamation-triangle']} className={`2xl:mr-2`} />
        <div className={`mt-2 2xl:mt-1 text-center`}>No records are found&nbsp;</div>
        <div className={`2xl:mt-1 text-center`}>for the username value (<span className={`font-mono text-hive-red`}>{searchRef.current}</span>).&nbsp;</div>
        <div className={`2xl:mt-1 text-center`}>Try another search.</div>
      </div>
      { renderSupportLink() }
      </>
    )
  }

  function renderClaimHistory(prevClaimData) {
    if (prevClaimData == null || prevClaimData.claims == null || prevClaimData.claims.length < 1 ) {
      return ( <p>No claim history</p> )
    }
    else {
      let claims
      let grandTotal
      claims = prevClaimData.claims
      grandTotal = prevClaimData.claimed_total
      let dateOptions = { year: 'numeric', month: 'short', day: '2-digit', hour: '2-digit', minute: '2-digit' }

      return (
        <>
        <p>Grand Total Claimed: {new Decimal(grandTotal).toFixed(3)} LARYNX</p>
        <div className={`flex justify-center flex-wrap`}>
        {claims.map((record, index) => (
          <div key={record + index} className={`text-xs rounded dark:bg-hive-light-black bg-hive-dark-grey p-3 m-3`}>
            <p>Claim Round {index + 1}</p>
            <p>{new Decimal(record.claim_amount_claimed).toFixed(3)} LARYNX</p>
            <p>Claim date: {record.claim_timestamp != null ? new Date(record.claim_timestamp).toLocaleString("en-US", dateOptions) : `N/A`}</p>
            {record.claim_timestamp != null ? (
              <>
              <p>Status: {record.processed ? `Complete` : `Processing`}</p>
              <p>Processed Time: {record.processed_timestamp != null ? new Date(record.processed_timestamp).toLocaleString("en-US", dateOptions) : `N/A`}</p>
              </>
            ) : (
              <>
              <p>Status: Unclaimed</p>
              <p>Processed Time: N/A</p>
              </>
            )}
          </div>
        ))}
        </div>
        </>
      )
    }
  }

  function renderSupportLink() {
    return (
      <p className={`flex flex-col md:flex-row justify-end space-x-2 w-full text-center md:mt-3 mt-6 text-sm`}>
        <span>need help?</span>
        <a target={`_blank`} href={`https://discord.com/channels/793757909837676554/915369046906658886`} rel={`noopener noreferrer`}>
          <span className={`link link-underline`}>get support! <FontAwesomeIcon icon={['fas', 'external-link-alt']} className={`fa-xs fa-fw`} /></span>
        </a>
      </p>
    )
  }
  

  function renderAvatar(pic) {
    const placeholderImg = () => {
      return(<img src='/images/hive-logo.svg' className={`rounded-full w-48px h-48px place-self-center bg-hive-light-grey`} />)
    }
    if(pic) {
      return(
      <span className={`place-self-center`}>
        {!avatarLoaded && placeholderImg()}
        <img src={pic} className={`rounded-full object-contain w-48px h-48px bg-hive-light-grey ${avatarLoaded ? `` : `hidden`}`} onLoad={() => setAvatarLoaded(true)} />
      </span>
      )
    }
    else {
      return(placeholderImg())
    }
  }

  function renderTextSearch() {
    return(
      <div className={`flex flex-col relative w-full sm:w-5/6 md:w-4/6 mb-4 mt-6 font-mono`}>
        <div className={`flex text-sm`}>Search:</div>
        <input
          id='search-input'
          type='text'
          tabIndex='1'
          placeholder='Search the Snapshot'
          title='Enter a Hive account username.'
          value={tempSearch}
          onChange={e => setTempSearch(e.target.value.toLowerCase().trim())}
          onKeyDown={onKeyDownSubmit.bind(this)}
          className={`w-full auth-form-input`}
        />
        {tempSearch != '' ? (
          <div className={`absolute right-20 top-1/2 self-center`}>
            <button id='search-clear' className={`flex mx-auto search-btn`} title='Clear Search' onClick={clearSearch}>
              <FontAwesomeIcon icon={['fas', 'times']} className={`mt-px`} fixedWidth />
            </button>
          </div>
        ) : ``}
        <div className={`absolute right-0 top-5 self-center`}>
          <button id='search-btn' className={`flex mx-auto btn btn-group-display-right justify-center btn-page-lg`} title='Submit Search' onClick={() => {
              getSnapPage(tempSearch)
            }}>
            <FontAwesomeIcon icon={['fas', 'search']} className={``} fixedWidth />
          </button>
        </div>
      </div>
    )
  }

  return (
    <Layout>
      <Head>
        <title>SPK Network Claim Drop - Hive Snapshot Lookup</title>
      </Head>
      <div className={` w-full lg:w-3/4 mx-auto`}>
        <h1 className={``}>Hive Snapshot Lookup</h1>
        <div className={`flex flex-col place-items-center`}>
          { renderTextSearch() }
          { doneLoadingSearch ?
            (currentSnapPage != null && currentSnapPage.data != null ?
              renderDisplay(currentSnapPage.data, claimHistory, userPeerDataRef.current) : ``
            ) :
            renderLoading()
          }
          { initialLoad ? renderSearchPrompt() : '' }
        </div>
      </div>
    </Layout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({ req }) {
    const { origin } = absoluteUrl(req, "localhost:3000")
    return { props: { origin } }
  }
)
