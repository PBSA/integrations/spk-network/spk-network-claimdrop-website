import { useUserContextState } from '../context/usercontext'
import Head from 'next/head'
import { useEffect, useRef, useState } from 'react'
import Layout from '../components/layout'
import Countdown, { zeroPad } from 'react-countdown'
import { useRouter } from 'next/router'
import { withSessionSsr } from '../lib/withSession'
import fetchJson from '../lib/fetchJson'
import absoluteUrl from 'next-absolute-url'

const renderer = ({ days, hours, minutes, seconds, completed }) => {
  if (completed) {
    return <p>Time Expired!</p>
  } else {
    let daysString = days == 1 ? 'day' : 'days'
    let hrsString = hours == 1 ? 'hour' : 'hours'
    let minsString = minutes == 1 ? 'minute' : 'minutes'
    let secsString = seconds == 1 ? 'second' : 'seconds'
    return (
      <p>{zeroPad(days)} {daysString}, {zeroPad(hours)} {hrsString}, {zeroPad(minutes)} {minsString}, {zeroPad(seconds)} {secsString}</p>
    )
  }
}

export default function Admin({ res, defaults, origin }) {
  const router = useRouter()
  const { user } = useUserContextState()
  const adminKey = useRef(res.adminKey)
  let arr = Object.entries(defaults)

  const [tempSettings, setTempSettings] = useState([])
  const [settings, setSettings] = useState(res.adminSettings)
  const [message, setMessage] = useState(res.message)
  const [logs, setLogs] = useState(null)
  //console.log(origin)

  useEffect(() => {
    // user logged out
    handleGetLog()
    if (user != null && user.isLoggedIn == false) {
      adminKey.current = ''
      router.push('/')
    }
  }, [user])

  async function handleReset(item, value = 'ALL') {
    if (value != '') {
      let body = {session: user}
      body[item] = value
      //console.log(JSON.stringify(body))
      let qReturn = await fetchJson(`${origin}/api/admin`, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
      })
      //console.log(qReturn)
      setSettings(qReturn.adminSettings)
      setMessage(qReturn.message)
      handleLog('reset ' + item + ' to default value.')
    }
  }

  async function handleSave(item, value) {
    if (value != '') {
      let body = {session: user}
      body[item] = value
      //console.log(JSON.stringify(body))
      let qReturn = await fetchJson(`${origin}/api/admin`, {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body)
        })
      //console.log(qReturn)
      setSettings(qReturn.adminSettings)
      setMessage(qReturn.message)
      handleLog('saved ' + item + ' as ' + value + '.')
    }
  }

  async function handleLog(action) {
    let body = {
      name: user.username,
      action: action
    }
    let qReturn = await fetchJson(`${origin}/api/admin/log`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
    //console.log(qReturn.logs)
    setLogs(qReturn.logs)
  }

  async function handleGetLog() {
    let qReturn = await fetchJson(`${origin}/api/admin/log`, {
      method: 'GET'
    })
    //console.log(qReturn.logs)
    setLogs(qReturn.logs)
  }

  async function handleDeleteLog() {
    let body = {
      name: user.username
    }
    let qReturn = await fetchJson(`${origin}/api/admin/log`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
    setLogs(qReturn.logs)
  }

  return (
    <Layout>
      <Head>
        <title>SPK Network Claim Drop - Admin Page</title>
      </Head>
      <div className={`py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black text-center shadow-lg rounded-lg my-10`}>
        <h2 className={``}>Website Settings</h2>
      </div>
      <div className={`flex justify-start py-6 px-8 bg-hive-dark-grey dark:bg-hive-light-black text-left font-mono shadow-lg rounded-lg my-10`}>
        <div className={`flex flex-col w-2/3 text-sm mt-3 mr-2`}>
          <p className={`text-left mb-2`}>Message: {message}</p>
          <button className={`btn btn-small ml-auto`} onClick={() => {handleReset('ALL', 'ALL')}}>
            RESET ALL
          </button>
          {arr.map(function (value, index, array) {
            return (
              <div key={value + index} className={`flex flex-col w-full mb-3 hover:drop-shadow-md hover:text-peerplays-dark-blue dark:hover:text-peerplays-light-blue`}>
                <p className={`w-full`}>{array[index][0]}: </p>
                <div className={`flex flex-row justify-between mb-1`}>
                  <p>Default = {array[index][1]}</p>
                  <p>Current = {settings[array[index][0]]}</p>
                  { Date.parse(array[index][1]) ? <Countdown date={settings[array[index][0]]} renderer={renderer} /> : `` }
                </div>
                <div className={`flex flex-row justify-between`}>
                  <input
                    id={`admin-input-${array[index][0]}`}
                    placeholder={array[index][1]}
                    value={tempSettings[array[index][0]]}
                    onChange={e => setTempSettings(e.target.value)}
                    className={`auth-form-input w-full h-32px pl-3 mr-3`}
                  />
                  <button className={`btn btn-small ml-auto mr-3`} onClick={() => {handleSave(array[index][0], document.getElementById(`admin-input-${array[index][0]}`).value)}}>
                    SAVE
                  </button>
                  <button className={`btn btn-small ml-auto`} onClick={() => {handleReset(array[index][0], array[index][1])}}>
                    RESET
                  </button>
                </div>
              </div>
            )
          })}
        </div>
        <div className={`flex flex-col w-1/3 text-sm mt-3 bg-hive-dark-black rounded-lg py-2 px-4 ml-2`}>
          <div className={`flex justify-between items-center mb-4 mt-2`}>
            <h4>Change Log</h4>
            <button className={`btn btn-small`} onClick={() => handleDeleteLog()}>
              DELETE
            </button>
          </div>
          {logs != null ? logs.map((record, index) => {
            return (
              <div key={record + index} className={`mb-2`}>
                <p>{new Date(record.timestamp).toISOString()}:</p>
                <p className={`ml-4`}>{record.name} {record.action}</p>
              </div>
            )
          }) : `No logs here!`}
        </div>
      </div>
    </Layout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({ req }) {
    const propsUser = req.session.user
    let log = null
    let res = { 
      message: 'Authorization Failed. Check the username / posting key and try again.',
      log: log,
      adminKey: 'invalid',
      adminSettings: null,
      callStatus: 401
    }

    let defaults = []

    const { origin } = absoluteUrl(req, "localhost:3000")

    if (propsUser != null && propsUser.isLoggedIn) {
      // is user an admin?
      res = await fetchJson(`${origin}/api/admin`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({session: propsUser})
      })

      let logBody = {
        name: propsUser.username,
        action: 'logged into admin page.'
      }
      log = await fetchJson(`${origin}/api/admin/log`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(logBody)
      })

      if (res.adminKey == 'invalid') {
        // admin verification failed.
        return {
          redirect: {
            destination: '/',
            permanent: false
          }
        }
      }
      else {
        // admin verification success.
        //console.log(res)
        defaults = {
          ADMIN_ACCOUNTS: process.env.ADMIN_ACCOUNTS,
          BLACKLISTED_ACCOUNTS: process.env.BLACKLISTED_ACCOUNTS,
          SPECIAL_ACCOUNTS: process.env.SPECIAL_ACCOUNTS,
          API_ENDPOINT_FETCH_CLAIM_BALANCES: process.env.API_ENDPOINT_FETCH_CLAIM_BALANCES,
          API_ENDPOINT_PEERPLAYS_NODE: process.env.API_ENDPOINT_PEERPLAYS_NODE,
          API_ENDPOINT_PEERPLAYS_FAUCET: process.env.API_ENDPOINT_PEERPLAYS_FAUCET,
          KEY_PREFIX: process.env.KEY_PREFIX,
          SNAPSHOT_DATETIME: process.env.SNAPSHOT_DATETIME,
          ICD_JAN_START_DATETIME: process.env.ICD_JAN_START_DATETIME,
          ICD_JAN_END_DATETIME: process.env.ICD_JAN_END_DATETIME,
          MCD_FEB_START_DATETIME: process.env.MCD_FEB_START_DATETIME,
          MCD_FEB_END_DATETIME: process.env.MCD_FEB_END_DATETIME,
          MCD_MAR_START_DATETIME: process.env.MCD_MAR_START_DATETIME,
          MCD_MAR_END_DATETIME: process.env.MCD_MAR_END_DATETIME,
          MCD_APR_START_DATETIME: process.env.MCD_APR_START_DATETIME,
          MCD_APR_END_DATETIME: process.env.MCD_APR_END_DATETIME,
          MCD_MAY_START_DATETIME: process.env.MCD_MAY_START_DATETIME,
          MCD_MAY_END_DATETIME: process.env.MCD_MAY_END_DATETIME,
          MCD_JUN_START_DATETIME: process.env.MCD_JUN_START_DATETIME,
          MCD_JUN_END_DATETIME: process.env.MCD_JUN_END_DATETIME,
          MCD_JUL_START_DATETIME: process.env.MCD_JUL_START_DATETIME,
          MCD_JUL_END_DATETIME: process.env.MCD_JUL_END_DATETIME,
          MCD_AUG_START_DATETIME: process.env.MCD_AUG_START_DATETIME,
          MCD_AUG_END_DATETIME: process.env.MCD_AUG_END_DATETIME,
          MCD_SEP_START_DATETIME: process.env.MCD_SEP_START_DATETIME,
          MCD_SEP_END_DATETIME: process.env.MCD_SEP_END_DATETIME,
          MCD_OCT_START_DATETIME: process.env.MCD_OCT_START_DATETIME,
          MCD_OCT_END_DATETIME: process.env.MCD_OCT_END_DATETIME,
          MCD_NOV_START_DATETIME: process.env.MCD_NOV_START_DATETIME,
          MCD_NOV_END_DATETIME: process.env.MCD_NOV_END_DATETIME,
          MCD_DEC_START_DATETIME: process.env.MCD_DEC_START_DATETIME,
          MCD_DEC_END_DATETIME: process.env.MCD_DEC_END_DATETIME
        }
        return { props: { res, defaults, origin } }
      }
    }
    else {
      // user is null or not logged in
      return {
        redirect: {
          destination: '/',
          permanent: false
        }
      }
    }
  }
)
